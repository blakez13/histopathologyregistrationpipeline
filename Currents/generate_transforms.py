import numpy as np


def main(block_objects, invivo_affine, invivo_translation, output_dir):

    with open(f'{output_dir}blender_z_offsets.txt', 'r') as f:
        z_dict = eval(f.read())

    invivo = np.eye(4)
    invivo[0:3, 0:3] = invivo_affine
    invivo[0:3, 3] = invivo_translation

    for block in block_objects:

        z_offset = np.eye(4)
        z_offset[2, 3] = z_dict[block.name]

        # Combine the affine transformations together
        stack = np.eye(4)
        stack[0:3, 0:3] = np.array(block.transforms['stacking']['affine'])
        stack[0:3, 3] = np.array(block.transforms['stacking']['translation'])

        exvivo = np.eye(4)
        exvivo[0:3, 0:3] = np.array(block.transforms['exvivo']['affine'])
        exvivo[0:3, 3] = np.array(block.transforms['exvivo']['translation'])

        refine = np.eye(4)
        refine[0:3, 0:3] = np.array(block.transforms['refine']['affine'])
        refine[0:3, 3] = np.array(block.transforms['refine']['translation'])

        # Apply the z-translation
        mat = np.matmul(stack, z_offset)
        # Now apply the exvivo
        mat = (np.matmul(exvivo, mat))
        # Now apply the refine
        mat = np.matmul(refine, mat)
        # Now apply to invivo
        mat = np.matmul(invivo, mat)

        # Now make this into a string
        flat_list = [item for sublist in mat.tolist() for item in sublist]

        with open(f'{output_dir}{block.name}/{block.name}_to_invivo_paraview.txt', 'w+') as f:
            f.write("\n".join(map(str, flat_list)))


if __name__ == '__main__':
    raise NotImplementedError
