import torch
import numpy as np
import torch.optim as optim
from Currents import energy
from Currents import IO
from Currents import plot

import matplotlib.pyplot as plt


class MeshObject:

    def __init__(self, surface_dict_list, name=None, transform_file=None):

        self.surface_dict = dict()
        self.mesh_mean = torch.tensor((0, 0, 0))
        if name:
            self.name = name
        for subset in surface_dict_list:
            if type(surface_dict_list[subset]) == str:
                self.surface_dict[subset] = IO.readobj(surface_dict_list[subset])
            else:
                self.surface_dict[subset] = surface_dict_list[subset]

        self._calc_normals()
        self._calc_centers()
        self._calc_means()

        if transform_file:
            self.transforms = IO.readdict(transform_file)

        # Set applied to False
        if transform_file:
            for step in self.transforms.keys():
                self.transforms[step]['applied'] = False

    def _calc_normals(self):

        for subset in self.surface_dict.keys():
            tris = self.surface_dict[subset]['vertices'][self.surface_dict[subset]['faces']]

            a = tris[:, 0, :]
            b = tris[:, 1, :]
            c = tris[:, 2, :]

            self.surface_dict[subset]['normals'] = 0.5 * torch.cross((a - b), (c - b), dim=1)

    def _calc_centers(self):

        for subset in self.surface_dict.keys():
            tris = self.surface_dict[subset]['vertices'][self.surface_dict[subset]['faces']]

            self.surface_dict[subset]['centers'] = (1 / 3.0) * tris.sum(1)

    def _calc_means(self):

        centers = torch.empty(0)
        for subset in self.surface_dict.keys():
            centers = torch.cat((centers, self.surface_dict[subset]['centers']))
            self.surface_dict[subset]['mean'] = self.surface_dict[subset]['centers'].mean(0)

        self.mesh_mean = centers.mean(0)

    def apply_stacking_(self, subsets=None):

        self._apply_affine_(
            self.transforms['stacking']['affine'],
            self.transforms['stacking']['translation'],
            subsets
        )
        self.transforms['stacking']['applied'] = True

    def apply_exvivo_(self, subsets=None):

        self._apply_affine_(
            self.transforms['exvivo']['affine'],
            self.transforms['exvivo']['translation'],
            subsets
        )
        self.transforms['exvivo']['applied'] = True

    def apply_refine_(self, subsets=None):

        self._apply_affine_(
            self.transforms['refine']['affine'],
            self.transforms['refine']['translation'],
            subsets
        )
        self.transforms['refine']['applied'] = True

    def _apply_affine_(self, affine, translation, subsets=None):

        if not subsets:
            subsets = self.surface_dict.keys()

        for subset in subsets:
            self.surface_dict[subset]['vertices'] = torch.mm(
                torch.tensor(affine, dtype=torch.float),
                self.surface_dict[subset]['vertices'].permute(1, 0)
            ).permute(1, 0)
            self.surface_dict[subset]['vertices'] += torch.tensor(translation, dtype=torch.float)

        self._calc_normals()
        self._calc_centers()
        self._calc_means()

    def flip_normals_(self, subset):

        self.surface_dict[subset]['normals'] *= -1

    def to_device_(self, subset, device):
        self.surface_dict[subset]['normals'] = self.surface_dict[subset]['normals'].to(device)
        self.surface_dict[subset]['centers'] = self.surface_dict[subset]['centers'].to(device)
        self.surface_dict[subset]['mean'] = self.surface_dict[subset]['mean'].to(device)
        self.mesh_mean = self.mesh_mean.to(device)

    def __len__(self, subset='surf'):

        length = 0
        for subset in self.surface_dict.keys():
            length += len(self.surface_dict[subset]['vertices'])
        return length


def match(src_surface, tar_surface, params, init_affine=None, device='cuda:1',
          rigid=False, display=True, translation=None):
    if init_affine is None:
        init_affine = torch.from_numpy(np.eye(3))

    # Accept lists - combine list surfaces to one surface
    # PROBLEM - NEED TO KNOW WHAT MEAN TO USE
    # THAT NEEDS TO BE INCLUDED IN DICT
    # MAYBE CALCULATE THE DISTANCE AND THE REMOVE ELEMENTS THAT ARE < T-HOLD
    # CHECK THAT THE MEANS FOR 'HEAD', 'FOOT', etc ARE ALL THE SAME
    if type(src_surface) == list:
        temp = {
            'vertices': src_surface[0]['vertices'],
            'faces': src_surface[0]['faces'],
            'normals': src_surface[0]['normals'],
            'centers': src_surface[0]['centers'],
            'mean': src_surface[0]['mean']
        }
        for surface in src_surface[1:]:
            # If the input is a list, make the list items into one surface
            temp['vertices'], temp['faces'], temp['normals'], temp['centers'] = IO.combine_surf(surface, temp)
            temp['mean'] = torch.cat((temp['mean'].unsqueeze(1), surface['mean'].unsqueeze(1)), 1).mean(1)
        src_surface = temp

    if type(tar_surface) == list:
        temp = {
            'vertices': tar_surface[0]['vertices'],
            'faces': tar_surface[0]['faces'],
            'normals': tar_surface[0]['normals'],
            'centers': tar_surface[0]['centers'],
            'mean': tar_surface[0]['mean']
        }
        for surface in tar_surface[1:]:
            # If the input is a list, make the list items into one surface
            temp['vertices'], temp['faces'], temp['normals'], temp['centers'] = IO.combine_surf(surface, temp)
        tar_surface = temp

    if display:
        # Plot the surfaces
        [_, fig, ax] = plot.plot_surface(
            tar_surface['vertices'],
            tar_surface['faces'],
        )
        ax.view_init(azim=-90.66532258064518, elev=-109.15514377763981)
        # ax.set_xlim(-30, 30)
        # ax.set_ylim(-10, 8)
        # fig.set_size_inches(8, 10)
        [src_mesh, _, _] = plot.plot_surface(
            src_surface['vertices'],
            src_surface['faces'],
            fig=fig,
            ax=ax,
            color=[1, 0, 0]
        )



    model, optimizer, affine, translation = get_model(
        tar_surface, src_surface, params, device, affine=init_affine, translation=translation
    )

    for epoch in range(0, params['n_iter']):

        optimizer.zero_grad()
        loss = model(
            src_surface['normals'],
            src_surface['centers'],
            tar_surface['normals'],
            tar_surface['centers'],
            affine,
            translation,
            params['sigma'],
            src_surface['mean'].to(device)
        )

        print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')

        loss.backward()  # Compute the gradients
        optimizer.step()  # Accumulates the gradients in optimizer

        # As we are stacking the blocks, make sure the transform is rigid
        if rigid:
            with torch.no_grad():
                U, _, V = affine.clone().svd()
                affine.data = torch.mm(U, V.transpose(1, 0))

        # if epoch % 10 == 0 and display:
        if display:
            plot.update_plot(
                src_mesh,
                src_surface['vertices'],
                src_surface['faces'],
                affine,
                translation,
                src_surface['mean']
            )
            plt.savefig('/home/sci/blakez/test_movie/image_%03d.png' % epoch, dpi=300)


    # Update the transforms for the source block
    affine = affine.detach().cpu().numpy()
    # Need to update the translation to account for not rotation about the origin
    translation = translation.detach().cpu().numpy()
    translation = -np.matmul(affine, src_surface['mean'].cpu().numpy()) + \
                  src_surface['mean'].cpu().numpy() + translation

    return affine, translation


def get_model(tar_surface_dict, src_surface_dict, param_dict, device,  optimizer=None, affine=None, translation=None):

    # Make sure the objects are on the GPU - This is not the same as before
    src_surface_dict['normals'] = src_surface_dict['normals'].to(device)
    src_surface_dict['centers'] = src_surface_dict['centers'].to(device)
    tar_surface_dict['normals'] = tar_surface_dict['normals'].to(device)
    tar_surface_dict['centers'] = tar_surface_dict['centers'].to(device)

    # Define the parameters to be optimized
    if affine is None:
        affine = torch.tensor(np.eye(3)).float()
    affine = affine.to(device).float()
    affine.requires_grad = True

    if translation is None:
        translation = (tar_surface_dict['mean'] - src_surface_dict['mean']).clone().float()
    translation = translation.to(device).float()
    translation.requires_grad = True

    # Create the loss function
    model = energy.CurrentsEnergy(
        tar_surface_dict['normals'],
        tar_surface_dict['centers'],
        param_dict['sigma'],
        kernel='cauchy'
    )

    # Create the optimizer
    if optimizer is None:
        optimizer = optim.SGD([
            {'params': affine, 'lr': param_dict['lr_affine']},
            {'params': translation, 'lr': param_dict['lr_translation']}],
            momentum=0.9, nesterov=True)

    return model, optimizer, affine, translation


def calc_nomrals(surface):

    tris = surface['vertices'][surface['faces']]

    a = tris[:, 0, :]
    b = tris[:, 1, :]
    c = tris[:, 2, :]

    return 0.5 * torch.cross((a - b), (c - b), dim=1)


def calc_centers(surface):

    tris = surface['vertices'][surface['faces']]
    return (1 / 3.0) * tris.sum(1)
