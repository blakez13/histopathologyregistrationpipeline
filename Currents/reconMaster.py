import os
import sys
import glob
import copy
import torch
import numpy as np
import matplotlib
import subprocess as sp

matplotlib.use('Qt5Agg')
# matplotlib.pyplot.ion()
from types import SimpleNamespace

from collections import OrderedDict
import torch.optim as optim
from Currents import plot
from Currents import IO
from Currents import energy


def read_mhd_header(filename):

    with open(filename, 'r') as in_mhd:
        long_string = in_mhd.read()

    short_strings = long_string.split('\n')
    key_list = [x.split(' = ')[0] for x in short_strings[:-1]]
    value_list = [x.split(' = ')[1] for x in short_strings[:-1]]
    a = OrderedDict(zip(key_list, value_list))

    return a


def write_mhd_header(filename, dictionary):
    long_string = '\n'.join(['{0} = {1}'.format(k, v) for k, v in dictionary.items()])
    with open(filename, 'w+') as out:
        out.write(long_string)


def _init_config(config_path, volume_path):
    step_dict = dict(
        affine=None,
        translation=None,
        lr_affine=0.000001,
        lr_translation=0.00001,
        sigma=5.0,
        n_iter=300,
        applied=False
    )

    main_dict = dict(
        config_path=config_path,
        volume_path=volume_path,
        stacking=copy.copy(step_dict),
        toExvivo=copy.copy(step_dict),
        refine=copy.copy(step_dict),
    )

    return main_dict


def block_stacking(block_objects, exvivo_object, device):

    # First thing we are going to do is match the exvivo_block1 and the first block

    # Define the parameters to be optimized
    init_affine = np.array([[0, -1, 0], [1, 0, 0], [0, 0, -1]])
    affine = torch.from_numpy(init_affine).float().to(device)
    affine.requires_grad = True
    # translation = torch.mv(affine, block_objects[0].surface_dict['exterior']['mean'].to(device))
    translation = (exvivo_object.surface_dict['block12']['mean'] - block_objects[0].surface_dict['exterior']['mean']).clone().float()
    translation = translation.to(device)
    translation.requires_grad = True

    # Create the loss function
    model = energy.CurrentsEnergy(
        exvivo_object.surface_dict['block12']['normals'],
        exvivo_object.surface_dict['block12']['centers'],
        block_objects[0].transforms['stacking']['sigma'],
        kernel='cauchy'
    )

    block_objects[0].to_device_('exterior', device)
    exvivo_object.to_device_('block12', device)

    # Create the optimizer
    optimizer = optim.SGD([
        {'params': affine, 'lr': block_objects[0].transforms['stacking']['lr_affine']},
        {'params': translation, 'lr': block_objects[0].transforms['stacking']['lr_translation']}],
        momentum=0.9, nesterov=True)

    for epoch in range(0, block_objects[0].transforms['stacking']['n_iter']):

        optimizer.zero_grad()
        loss = model(
            block_objects[0].surface_dict['exterior']['normals'],
            block_objects[0].surface_dict['exterior']['centers'],
            exvivo_object.surface_dict['block12']['normals'],
            exvivo_object.surface_dict['block12']['centers'],
            affine,
            translation,
            block_objects[0].transforms['stacking']['sigma'],
            block_objects[0].surface_dict['exterior']['mean']
        )

        print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')

        loss.backward()  # Compute the gradients
        optimizer.step()  # Accumulates the gradients in optimizer

        # As we are stacking the blocks, make sure the transform is rigid
        # with torch.no_grad():
        #     U, s, V = affine.clone().svd()
        #     # Just constrain the z-direction affine scaling
        #     # s[0] = 1.0
        #     affine.data = torch.mm(U, (torch.mm(torch.diag(s), V.transpose(1, 0))))

    # Update the transforms for the source block
    affine = affine.detach().cpu().numpy()
    # Need to update the translation to account for not rotation about the origin
    translation = translation.detach().cpu().numpy()
    translation = -np.matmul(affine, block_objects[0].surface_dict['exterior']['mean'].cpu().numpy()) + \
                  block_objects[0].surface_dict['exterior']['mean'].cpu().numpy() + translation

    block_objects[0].transforms['stacking']['affine'] = affine.tolist()
    block_objects[0].transforms['stacking']['translation'] = translation.tolist()

    # Now apply the affine and write it out
    block_objects[0].apply_affine_(
        block_objects[0].transforms['stacking']['affine'],
        block_objects[0].transforms['stacking']['translation']
    )

    IO.writeobj(
        block_objects[0].surface_dict['exterior']['vertices'],
        block_objects[0].surface_dict['exterior']['faces'],
        '/hdscratch/ucair/blockface/18_047/surfaces/rigid_stacked/block12/transformed_to_exvivo_exterior.obj'
    )

    IO.writedict(block_objects[0].transforms, block_objects[0].transforms['config_path'])

    iterator = range(0, len(block_objects))

    for ii, src_block, tar_block in zip(iterator[:-1][::-1], block_objects[1:], block_objects[:-1]):

        # Apply the previous affine to the target block
        # if ii == 0:
        #     tar_block.apply_affine_(
        #         tar_block.transforms['stacking']['affine'],
        #         tar_block.transforms['stacking']['translation'],
        #     )
        # else:
        #     tar_block.apply_affine_(
        #         tar_block.transforms['stacking']['affine'],
        #         tar_block.transforms['stacking']['translation'],
        #         subsets=['foot', 'head', 'exterior']
        #     )

        # # If there is a support piece in the target block, now is the time to append it
        # if 'support' in tar_block.surface_dict:
        #     # # We need to apply the supporting affine to the support face
        #     # tar_block.apply_affine_(
        #     #     block_objects[ii - 1].transforms['stacking']['affine'],
        #     #     block_objects[ii - 1].transforms['stacking']['translation'],
        #     #     subsets=['support']
        #     # )
        #
        #     comb_verts, comb_faces = IO.combine_arbitrary(
        #         tar_block.surface_dict['support']['vertices'],
        #         tar_block.surface_dict['foot']['vertices'],
        #         tar_block.surface_dict['support']['faces'],
        #         tar_block.surface_dict['foot']['faces'],
        #     )
        #     # Now make the 'foot' equal to the combined ones
        #     tar_block.surface_dict['foot']['vertices'] = comb_verts
        #     tar_block.surface_dict['foot']['faces'] = comb_faces
        #
        #     # We need to make sure the normals and centers are updates as well
        #     tar_block._calc_normals()
        #     tar_block._calc_centers()
        #     tar_block._calc_means()

        if src_block.transforms['stacking']['affine']:
            # Plot the surfaces
            [_, fig, ax] = plot.plot_surface(
                tar_block.surface_dict['head']['vertices'],
                tar_block.surface_dict['head']['faces'],
            )
            [src_mesh, _, _] = plot.plot_surface(
                src_block.surface_dict['foot']['vertices'],
                src_block.surface_dict['foot']['faces'],
                fig=fig,
                ax=ax,
                color=[1, 0, 0]
            )

            # Need to move the normals and the centers to the gpu
            src_block.to_device_('foot', device)
            src_block.to_device_('exterior', device)
            exvivo_object.to_device_('exterior', device)
            tar_block.to_device_('head', device)

            # Define the parameters to be optimized
            affine = torch.from_numpy(init_affine).float().cuda(device)
            affine.requires_grad = True
            translation = (tar_block.surface_dict['head']['mean'] - src_block.surface_dict['foot']['mean']).clone().float()
            translation = translation.cuda(device)
            translation.requires_grad = True

            # Create the loss function
            model = energy.CurrentsEnergy(
                tar_block.surface_dict['head']['normals'],
                tar_block.surface_dict['head']['centers'],
                src_block.transforms['stacking']['sigma'],
                kernel='cauchy'
            )

            # Create the optimizer
            optimizer = optim.SGD([
                {'params': affine, 'lr': src_block.transforms['stacking']['lr_affine']/10},
                {'params': translation, 'lr': src_block.transforms['stacking']['lr_translation']}],
                momentum=0.9, nesterov=True)

            for epoch in range(0, src_block.transforms['stacking']['n_iter']):

                optimizer.zero_grad()
                loss = model(
                    src_block.surface_dict['foot']['normals'],
                    src_block.surface_dict['foot']['centers'],
                    tar_block.surface_dict['head']['normals'],
                    tar_block.surface_dict['head']['centers'],
                    affine,
                    translation,
                    src_block.transforms['stacking']['sigma'],
                    src_block.surface_dict['foot']['mean']
                )

                loss += model(
                    src_block.surface_dict['exterior']['normals'],
                    src_block.surface_dict['exterior']['centers'],
                    exvivo_object.surface_dict['exterior']['normals'],
                    exvivo_object.surface_dict['exterior']['centers'],
                    affine,
                    translation,
                    src_block.transforms['stacking']['sigma'],
                    src_block.surface_dict['foot']['mean']
                )

                print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}  index:')

                loss.backward()  # Compute the gradients
                optimizer.step()  # Accumulates the gradients in optimizer

                # As we are stacking the blocks, make sure the transform is rigid
                with torch.no_grad():
                    U, s, V = affine.clone().svd()
                    # Just constrain the z-direction affine scaling
                    # index = torch.mv(V.transpose(1,0), torch.tensor([0, 0, 1]).float().to(device)).abs().argmax()
                    s[2] = 1.0
                    affine.data = torch.mm(U, (torch.mm(torch.diag(s), V.transpose(1,0))))
                    # print(torch.mv(V.transpose(1,0), torch.tensor([0, 0, 1]).float().to(device)).abs())

                if epoch % 10 == 0:
                    plot.update_plot(
                        src_mesh,
                        src_block.surface_dict['foot']['vertices'],
                        src_block.surface_dict['foot']['faces'],
                        affine,
                        translation,
                        src_block.surface_dict['foot']['mean']
                    )

            # Update the transforms for the source block
            affine = affine.detach().cpu().numpy()
            # Need to update the translation to account for not rotation about the origin
            translation = translation.detach().cpu().numpy()
            translation = -np.matmul(affine, src_block.surface_dict['foot']['mean'].cpu().numpy()) + \
                          src_block.surface_dict['foot']['mean'].cpu().numpy() + translation

            src_block.transforms['stacking']['affine'] = affine.tolist()
            src_block.transforms['stacking']['translation'] = translation.tolist()

            # Now apply the affine and write it out
            src_block.apply_affine_(
                src_block.transforms['stacking']['affine'],
                src_block.transforms['stacking']['translation']
            )

            IO.writeobj(
                src_block.surface_dict['exterior']['vertices'],
                src_block.surface_dict['exterior']['faces'],
                f'/hdscratch/ucair/blockface/18_047/surfaces/rigid_stacked/block{ii+1:02d}/transformed_to_exvivo_exterior.obj'
            )

            IO.writedict(src_block.transforms, src_block.transforms['config_path'])

    # # All the other blocks have had their affines applied already
    # src_block.apply_affine_(
    #     src_block.transforms['stacking']['affine'],
    #     src_block.transforms['stacking']['translation']
    # )


def stacked_to_exvivo(exvivo, block_objects, device, init_affine=np.eye(3)):

    # Because of how the surfaces were generated, need to flip the direction of the exterior normals
    # Need to get the center of all the block exteriors for the translation and the mean
    centers = torch.empty(0).cuda(device)
    for block in block_objects:
        block.flip_normals_('exterior')  # We did this earlier for consistency
        # Need to move the normals and the centers to the gpu
        block.to_device_('exterior', device)
        centers = torch.cat((centers, block.surface_dict['exterior']['centers']))
    exterior_surface_mean = centers.mean(0).cuda(device)

    # Move the exvivo to cuda
    exvivo.to_device_('exterior', device)

    # Define the parameters to be optimized
    affine = torch.from_numpy(init_affine).float().cuda(device)
    affine.requires_grad = True
    translation = (exvivo.surface_dict['exterior']['mean'] - exterior_surface_mean).clone().float()
    translation = translation.cuda(device)
    translation.requires_grad = True

    # Create the loss function
    model = energy.CurrentsEnergy(
        exvivo.surface_dict['exterior']['normals'],
        exvivo.surface_dict['exterior']['centers'],
        block_objects[0].transforms['toExvivo']['sigma']
    )

    # Create the optimizer
    optimizer = optim.SGD([
        {'params': affine, 'lr': block_objects[0].transforms['toExvivo']['lr_affine']},
        {'params': translation, 'lr': block_objects[0].transforms['toExvivo']['lr_translation']}],
        momentum=0.9, nesterov=True)

    for epoch in range(0, block_objects[0].transforms['toExvivo']['n_iter']):
        loss = 0
        optimizer.zero_grad()
        for block in block_objects:
            loss += model(
                block.surface_dict['exterior']['normals'],
                block.surface_dict['exterior']['centers'],
                exvivo.surface_dict['exterior']['normals'],
                exvivo.surface_dict['exterior']['centers'],
                affine,
                translation,
                block.transforms['toExvivo']['sigma'],
                exterior_surface_mean
            )

        loss.backward()
        optimizer.step()

        print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')

    # Update the transforms for the source block
    affine = affine.detach().cpu().numpy()
    # Need to update the translation to account for not rotation about the origin
    translation = translation.detach().cpu().numpy()
    translation = -np.matmul(affine, exterior_surface_mean.cpu().numpy()) + \
                  exterior_surface_mean.cpu().numpy() + translation

    for block in block_objects:
        block.transforms['toExvivo']['affine'] = affine.tolist()
        block.transforms['toExvivo']['translation'] = translation.tolist()
        IO.writedict(block.transforms, block.transforms['config_path'])


def refine_stack(exvivo, block_objects, device):

    ### Temporary ###
    for block in block_objects:
        block.transforms['refine']['sigma_exterior'] = 5.0
        block.transforms['refine']['sigma_blocks'] = 0.5
        block.transforms['refine']['lr_affine'] = 0.0000002
        block.transforms['refine']['lr_translation'] = 0.0000002
        block.transforms['refine']['n_iter'] = 200

    # Make sure all of the surfaces are on the GPU
    for i, block in enumerate(block_objects, 1):
        block.to_device_('exterior', device)
        if i > 1:
            block.to_device_('head', device)
            block.flip_normals_('head')
        if i < len(block_objects):
            block.to_device_('foot', device)
    exvivo.to_device_('exterior', device)

    # For some reason the exvivo is flipped
    exvivo.flip_normals_('exterior')

    # Define the parameters to be optimized
    affine = torch.eye(3, dtype=torch.float, device=device, requires_grad=False).repeat(len(block_objects), 1, 1)
    affine.requires_grad = True  # Have to do this after the repeat
    translation = torch.zeros((len(block_objects), 1, 3), dtype=torch.float, device=device, requires_grad=True)

    # Create the loss function

    exteriorModel = energy.CurrentsEnergy(
        exvivo.surface_dict['exterior']['normals'],
        exvivo.surface_dict['exterior']['centers'],
        block_objects[0].transforms['refine']['sigma_exterior'],
        kernel='cauchy'
    )
    blockModel = energy.CurrentsEnergy(
        torch.cat([x.surface_dict['head']['normals'] for x in block_objects[1:]], 0),
        torch.cat([x.surface_dict['head']['centers'] for x in block_objects[1:]], 0),
        block_objects[0].transforms['refine']['sigma_blocks'],
        kernel='cauchy'
    )

    # Create the optimizer
    optimizer = optim.SGD([
        {'params': affine, 'lr': block_objects[0].transforms['refine']['lr_affine']},
        {'params': translation, 'lr': block_objects[0].transforms['refine']['lr_translation']}],
        momentum=0.9, nesterov=True)

    se = 0.5
    sf = 1.0

    for epoch in range(0, block_objects[0].transforms['refine']['n_iter']):
        loss = 0
        optimizer.zero_grad()

        # Calculate the energy of each block with the exvivo
        for b, block in enumerate(block_objects, 0):
            loss += se * exteriorModel(
                block.surface_dict['exterior']['normals'],
                block.surface_dict['exterior']['centers'],
                exvivo.surface_dict['exterior']['normals'],
                exvivo.surface_dict['exterior']['centers'],
                affine[b],
                translation[b],
                block.transforms['refine']['sigma_exterior'],
                block.mesh_mean
            )

        # Calculate the energy of the faces with the appropriate neighbors
        loss += sf * blockModel(
                block_objects[0].surface_dict['foot']['normals'],
                block_objects[0].surface_dict['foot']['centers'],
                block_objects[1].surface_dict['head']['normals'],
                block_objects[1].surface_dict['head']['centers'],
                affine[0],
                translation[0],
                block_objects[0].transforms['refine']['sigma_blocks'],
                block_objects[0].mesh_mean
            )

        for i, (front, middle, back) in enumerate(zip(block_objects[:-2], block_objects[1:-1], block_objects[2:]), 1):
            #
            # # Don't union the surfaces, but calculate them separately so there is no overlap with large sigma
            loss += sf * blockModel(
                middle.surface_dict['head']['normals'],
                middle.surface_dict['head']['centers'],
                front.surface_dict['foot']['normals'],
                front.surface_dict['foot']['centers'],
                affine[i],
                translation[i],
                block_objects[i].transforms['refine']['sigma_blocks'],
                block_objects[i].mesh_mean
            )

            loss += sf * blockModel(
                middle.surface_dict['foot']['normals'],
                middle.surface_dict['foot']['centers'],
                back.surface_dict['head']['normals'],
                back.surface_dict['head']['centers'],
                affine[i],
                translation[i],
                block_objects[i].transforms['refine']['sigma_blocks'],
                block_objects[i].mesh_mean
            )

        loss += sf * blockModel(
            block_objects[-1].surface_dict['head']['normals'],
            block_objects[-1].surface_dict['head']['centers'],
            block_objects[-2].surface_dict['foot']['normals'],
            block_objects[-2].surface_dict['foot']['centers'],
            affine[-1],
            translation[-1],
            block_objects[-1].transforms['refine']['sigma_blocks'],
            block_objects[-1].mesh_mean
        )

        loss.backward()
        optimizer.step()

        # with torch.no_grad():
        #     temp = affine.clone()
        #     for a in range(0, len(block_objects)):
        #         # As we are stacking the blocks, make sure the transform is rigid
        #         U, _, V = temp[a].clone().svd()
        #         temp[a] = torch.mm(U, V.transpose(1, 0))
        #     affine.data = temp.clone()

        print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')

    for i, block in enumerate(block_objects, 0):
        # Update the transforms for the source block
        affine_block = affine[i].clone().detach().cpu().numpy()
        # Need to update the translation to account for not rotation about the origin
        translation_block = translation[i].clone().detach().cpu().numpy()
        translation_block = -np.matmul(affine_block, block.mesh_mean.cpu().numpy()) + \
                                       block.mesh_mean.cpu().numpy() + translation_block

        block.transforms['refine']['affine'] = affine_block.tolist()
        block.transforms['refine']['translation'] = translation_block.tolist()
        IO.writedict(block.transforms, block.transforms['config_path'])


def apply_affines(block_objects, block_list):

    for i, block in enumerate(block_objects):

        if i < 5:
            pass
        else:
            # Combine the affine transformations together
            stack = np.eye(4)
            stack[0:3, 0:3] = np.array(block.transforms['stacking']['affine'])
            stack[0:3, 3] = np.array(block.transforms['stacking']['translation'])

            # toExvivo = np.eye(4)
            # toExvivo[0:3, 0:3] = np.array(block.transforms['toExvivo']['affine'])
            # toExvivo[0:3, 3] = np.array(block.transforms['toExvivo']['translation'])
            #
            # refine = np.eye(4)
            # refine[0:3, 0:3] = np.array(block.transforms['refine']['affine'])
            # refine[0:3, 3] = np.array(block.transforms['refine']['translation'])
            #
            # affine = np.matmul(refine, (np.matmul(toExvivo, stack)))
            affine = stack
            print(affine)
            affine_string = ''

            image_file = f'{block_list[i]}/../../../affineImages/{block_list[i].split("/")[-1]}/difference_volume.mhd'
            # update the transform matrix (shouldn't have anything right now)
            # output_file = '/home/sci/blakez/test_outmhd.mhd'
            output_file = f'{block_list[i]}/../../../affineImages/{block_list[i].split("/")[-1]}/affine_tformed.nrrd'

            z_offset = float(input('Please input the initial Z offset from blender: '))

            print(f"===> Applying transforms to {block_list[i].split('/')[-1]}")
            # Need to run the apply affine in python 2 with PyCA
            p = sp.Popen(["/usr/bin/python",
                          "-u",
                          "/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/ApplyBlockAffines.py",
                          "-b", f"{image_file}",
                          "-a", f"{affine_string.join([f' {x}'for x in affine.flatten()])}",
                          "-z", f"{z_offset}",
                          "-o", f"{output_file}"],
                          # "-m", f"{mask_file}"],
                         stdout=sp.PIPE)
            sout, _ = p.communicate()
            p.wait()

            print(sout.decode('utf-8'))
            sys.stdout.flush()

def write_transform():
    raise NotImplementedError


def main(blockList):
    # Clean the block list so we only get a list of the blocks for the rabbit we want
    # blockList = [x for x in blockList if rabbit in x]
    block_objects = []

    # Make a directory for the rigid stacked
    if not os.path.exists(f'{blockList[0]}/../../rigid_stacked/'):
        os.makedirs(f'{blockList[0]}/../../rigid_stacked/')

    # Load the exvivo surface
    exvivo_dict = {
        'exterior': '/home/sci/blakez/ucair/18_047/rawVolumes/ExVivo_2018-07-26/Exvivo_surface_decimate.obj',
        'block01': '/home/sci/blakez/ucair/18_047/rawVolumes/ExVivo_2018-07-26/Exvivo_surface_block01.obj',
        'block12': '/home/sci/blakez/ucair/18_047/rawVolumes/ExVivo_2018-07-26/Exvivo_surface_block12.obj'
    }
    exvivo = IO.MeshObject(exvivo_dict)

    # Generate Mesh objects for each of the blocks
    for ii, block in enumerate(blockList, 1):

        # Check to see if a transform file exists
        transform = f'{block}/../../rigid_stacked/{block.split("/")[-1]}/{block.split("/")[-1]}_stacking_config.yaml'
        if not os.path.exists(transform):
            os.makedirs(os.path.dirname(transform))
            config = _init_config(transform, block)
            IO.writedict(config, transform)

        objects = sorted(glob.glob(block + '/*.obj'))
        subsets = [x for x in objects if any(ext in x for ext in ['_head.obj', '_foot.obj',
                                                                  '_exterior.obj', '_support.obj'])]
        block_dict = dict()

        block_dict['exterior'] = subsets[0]

        if ii == 1:
            block_dict['foot'] = subsets[-1]

        elif ii == len(blockList):
            block_dict['head'] = subsets[-1]

        else:
            block_dict['foot'] = subsets[1]
            block_dict['head'] = subsets[2]

        if len(subsets) > 3:
            block_dict['support'] = subsets[3]

        block_objects.append(IO.MeshObject(block_dict, block, transform_file=transform))

    # For 18_022, the way the mesh was generated means that the exterior and head normals need to be flipped
    for i, block in enumerate(block_objects, 1):
        block.flip_normals_('exterior')
        if i < 12:
            block.flip_normals_('foot')

    # Check if the blocks already have been stacked - make sure all blocks have an affine transformation
    # stacked = [True if x.transforms['stacking']['affine'] else False for x in block_objects]
    # if any(stacked):
    # block_stacking(block_objects[::-1], exvivo, 'cuda:1')

    # Actaully have to apply the affines now

    apply_affines(block_objects, blockList)

    sys.exit()

    # Write out the transformed exterior surfaces to confirm correct initialization
    exterior_verts, exterior_faces = IO.combine_surfaces(block_objects, 'exterior')
    IO.writeobj(exterior_verts.numpy(), exterior_faces.numpy(), f'{blockList[0]}/../../rigid_stacked/exterior_comb.obj')

    if not os.path.exists(f'{blockList[0]}/../../rigid_stacked/exterior_comb_init.obj'):
        # Now register the stacked blocks to the exvivo surface
        init_affine = np.array([[0, -1, 0], [1, 0, 0], [0, 0, -1]])
        # for block in block_objects:
        #     block.apply_affine_(torch.from_numpy(init_affine), torch.from_numpy(np.array([0,0,0])), subsets=['exterior'])
        # exterior_verts, exterior_faces = IO.combine_surfaces(block_objects, 'exterior')
        # IO.writeobj(exterior_verts.numpy(), exterior_faces.numpy(),
        #             f'{blockList[0]}/../../rigid_stacked/exterior_comb_init.obj')
        # Determined from Blender
        stacked_to_exvivo(exvivo, block_objects, 'cuda:1', init_affine)
    # Apply the to exvivo affine for each of the blocks
    for block in block_objects:
        block.apply_affine_(block.transforms['toExvivo']['affine'], block.transforms['toExvivo']['translation'])

    # Write out the transformed exterior surfaces to confirm correct initialization
    exterior_verts, exterior_faces = IO.combine_surfaces(block_objects, 'exterior')
    IO.writeobj(exterior_verts.numpy(), exterior_faces.numpy(), '/home/sci/blakez/test_to_exvivo_exterior_affine.obj')

    refined = [True if x.transforms['refine']['affine'] else False for x in block_objects]
    if not any(refined):
        refine_stack(exvivo, block_objects, 'cuda:1')

        # Apply the to refine affine for each of the blocks
        for block in block_objects:
            block.apply_affine_(block.transforms['refine']['affine'], block.transforms['refine']['translation'])

        # Write out the transformed exterior surfaces to confirm correct initialization
        exterior_verts, exterior_faces = IO.combine_surfaces(block_objects, 'exterior')
        IO.writeobj(exterior_verts.numpy(), exterior_faces.numpy(), '/home/sci/blakez/test_to_exvivo_exterior_refined_affine.obj')


    # # Now we need to actually compose and apply the transformations
    apply_affines(block_objects, blockList)


    print('===> Done')


if __name__ == '__main__':

    # Dir = '/home/sci/blakez/ucair/histopathology/'
    Dir = '/hdscratch/ucair/blockface/18_047/surfaces/raw/'
    # Get a list of all the blocks for all the rabbits
    rabbits = sorted(glob.glob(Dir + '/18*'))
    out = '/home/sci/blakez/ucair/histopathology/vnetData/'
    # blockList = []
    blockList = sorted(glob.glob(Dir + '*'))
    # # Get a list of all the block
    # for rabbit in rabbits:
    #     blockList += sorted(glob.glob(rabbit + '/blockface/volumes/*'))
    main(blockList)
