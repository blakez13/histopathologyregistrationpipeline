import os
import sys
import glob
import copy
import torch
import argparse
import numpy as np
import subprocess as sp

# import matplotlib
# matplotlib.use('Qt5Agg')
# matplotlib.pyplot.ion()

from collections import OrderedDict
import torch.optim as optim
from Currents import plot
from Currents import IO
from Currents import energy
from Currents import currents
#


def apply_affine(A, T, args):

    # Combine the affine transformations together
    affine = np.eye(4)
    affine[0:3, 0:3] = np.array(A)
    affine[0:3, 3] = np.array(T)
    affine_string = ''

    image_file = f'{args.exvivo}011_----_3D_VIBE_0p5iso_cor_3ave.nii.gz'
    # update the transform matrix (shouldn't have anything right now)
    # output_file = '/home/sci/blakez/test_outmhd.mhd'
    output_file = f'/home/sci/blakez/output_exvivo.nii.gz'

    # Need to run the apply affine in python 2 with PyCA
    p = sp.Popen(["/usr/bin/python",
                  "-u",
                  "/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/ApplyBlockAffines.py",
                  "-b", f"{image_file}",
                  "-a", f"{affine_string.join([f' {x}'for x in affine.flatten()])}",
                  "-o", f"{output_file}"],
                  # "-m", f"{mask_file}"],
                 stdout=sp.PIPE)
    sout, _ = p.communicate()
    p.wait()


    image_file = f'{args.exvivo}012_----_t2_spc_1mm_iso_satBand_3p5ave.nii.gz'
    # update the transform matrix (shouldn't have anything right now)
    # output_file = '/home/sci/blakez/test_outmhd.mhd'
    output_file = f'/home/sci/blakez/output_exvivo_t2.nii.gz'

    # Need to run the apply affine in python 2 with PyCA
    p = sp.Popen(["/usr/bin/python",
                  "-u",
                  "/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/ApplyBlockAffines.py",
                  "-b", f"{image_file}",
                  "-a", f"{affine_string.join([f' {x}' for x in affine.flatten()])}",
                  "-o", f"{output_file}"],
                 # "-m", f"{mask_file}"],
                 stdout=sp.PIPE)
    sout, _ = p.communicate()
    p.wait()


    print(sout.decode('utf-8'))
    sys.stdout.flush()


def main(invivo_dir, exvivo_dir):

    params = dict(
        affine=None,
        translation=None,
        lr_affine=0.000001,
        lr_translation=0.00001,
        sigma=5.0,
        n_iter=300,
        applied=False
    )

    # Find the ablation region in exvivo
    exvivo_path = glob.glob(exvivo_dir + '*.obj')
    subsets = [x for x in exvivo_path if 'ablation' in x and 'decimate' in x]
    exvivo_dict = dict()
    for subset in subsets:
        exvivo_dict['ablation'] = subset
    exvivo = currents.MeshObject(exvivo_dict)

    # Find the ablation region in invivo
    invivo_path = glob.glob(invivo_dir + '*.obj')
    invivo_path = [x for x in invivo_path if 'ablation' in x and 'decimate' in x]
    invivo_dict = {'ablation': invivo_path[0]}
    invivo = currents.MeshObject(invivo_dict)

    A, T = currents.match(
        exvivo.surface_dict['ablation'],
        invivo.surface_dict['ablation'],
        params,
        rigid=False,
        display=True
    )
    if __name__ == '__main__':
        apply_affine(A, T, args)

    return A, T


if __name__ == '__main__':
    # # Training settings
    parser = argparse.ArgumentParser(description='PyTorch Super Res Example')
    parser.add_argument('-e', '--exvivo', type=str, required=True, help="Path to the exvivo directory")
    parser.add_argument('-i', '--invivo', type=str, required=True, help="Path to the invivo directory")
    # # parser.add_argument('--label_directory', type=str, required=True, help="Top directory for the labels")
    # # parser.add_argument('--train_batch', type=int, default=8, help='training batch size')
    # # parser.add_argument('--test_batch', type=int, default=16, help='testing batch size')
    # # parser.add_argument('--nEpochs', type=int, default=500, help='number of epochs to train for')
    # # parser.add_argument('--lr', type=float, default=0.00001, help='Learning Rate. Default=0.01')
    # # parser.add_argument('--split', type=float, default=0.8, help='Amount of data to use for training (0,1]')
    # # parser.add_argument('--cuda', action='store_true', help='use cuda?')
    # # parser.add_argument('--augmentation_factor', type=int, default=10, help='Number of times to sample original list')
    # # parser.add_argument('--threads', type=int, default=30, help='number of threads for data loader to use')
    # # parser.add_argument('--scheduler', action='store_true', help='use scheduler?')
    # # parser.add_argument('--seed', type=int, default=1234, help='random seed to use. Default=123')
    #
    args = parser.parse_args()

    _, _ = main(args.invivo, args.exvivo)

