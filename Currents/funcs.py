import torch
import numpy as np
import SimpleITK as sitk

from Currents import IO


def colorize(surface_dict, subset, block_path):
    # Need to read in the color volume
    volume_reader = sitk.ImageFileReader()
    volume_reader.SetFileName(f'{block_path}/{block_path.split("/")[-1]}_volume_color.mha')
    color_volume = volume_reader.Execute()

    # Also annoyingly need the black and white volume that has been localized
    volume_reader.SetFileName(f'{block_path}/{block_path.split("/")[-1]}_volume_zLoc.nrrd')
    gray_volume = volume_reader.Execute()

    colors = []
    print(f'Coloring the vertices ... ', end='')
    z_sizes = np.array([0, gray_volume.GetSize()[2]])
    for vert in surface_dict['vertices']:
        index = np.round(gray_volume.TransformPhysicalPointToContinuousIndex(vert.numpy().tolist()))
        index = index.astype(int)
        try:
            color_volume.GetPixel(index.tolist())[0:3]
        except:
            # Just set the Z to the one that is closest
            if abs(0 - index[2]) <= 1.0:
                index[2] = 0
            elif abs(gray_volume.GetSize()[2] - index[2]) <= 1.0:
                index[2] = gray_volume.GetSize()[2] - 1
            else:
                Exception('Too Far out of bounds!')
        colors.append(list(color_volume.GetPixel(index.tolist())[0:3]))

    colors = torch.Tensor(colors)
    surface_dict['colors'] = colors

    print('Done')

    IO.writedict({'colors': colors}, f'{block_path}/{subset}_vertices_colors.yml')
