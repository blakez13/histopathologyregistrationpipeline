import os
import sys
import glob
import copy
import torch
import argparse
import numpy as np
import subprocess as sp

# import matplotlib
# matplotlib.use('Qt5Agg')
# matplotlib.pyplot.ion()

from collections import OrderedDict
import torch.optim as optim
from Currents import plot
from Currents import IO
from Currents import energy
from Currents import currents
from Currents import exvivo_invivo
from Currents import generate_transforms

# Training settings
parser = argparse.ArgumentParser(description='PyTorch Super Res Example')
parser.add_argument('-d', '--directory', type=str, required=True, help="Base directory for the rabbit")
parser.add_argument('-r', '--rabbit', type=str, required=True, help="Rabbit to reconstruct")
parser.add_argument('-e', '--exvivo', type=str, required=True, help="Path to the exvivo directory")
# parser.add_argument('--label_directory', type=str, required=True, help="Top directory for the labels")
# parser.add_argument('--train_batch', type=int, default=8, help='training batch size')
# parser.add_argument('--test_batch', type=int, default=16, help='testing batch size')
# parser.add_argument('--nEpochs', type=int, default=500, help='number of epochs to train for')
# parser.add_argument('--lr', type=float, default=0.00001, help='Learning Rate. Default=0.01')
# parser.add_argument('--split', type=float, default=0.8, help='Amount of data to use for training (0,1]')
# parser.add_argument('--cuda', action='store_true', help='use cuda?')
# parser.add_argument('--augmentation_factor', type=int, default=10, help='Number of times to sample original list')
# parser.add_argument('--threads', type=int, default=30, help='number of threads for data loader to use')
# parser.add_argument('--scheduler', action='store_true', help='use scheduler?')
# parser.add_argument('--seed', type=int, default=1234, help='random seed to use. Default=123')

args = parser.parse_args()


def init_config(config_path, volume_path):
    step_dict = dict(
        affine=None,
        translation=None,
        lr_affine=0.000001,
        lr_translation=0.00001,
        sigma=5.0,
        n_iter=300,
        applied=False
    )

    main_dict = dict(
        stacking=copy.copy(step_dict),
        exvivo=copy.copy(step_dict),
        refine=copy.copy(step_dict),
    )

    return main_dict


def rigid_stack(block_objects, exvivo_object, out_dir):

    # First register the first block with the exvivo
    # The inital affine is determined from blender
    init_affine = torch.tensor([[0, -1, 0], [1, 0, 0], [0, 0, -1]])

    A, T = currents.match(
        block_objects[0].surface_dict['exterior'],
        exvivo_object.surface_dict[block_objects[0].name],
        block_objects[0].transforms['stacking'],
        init_affine=init_affine,
        rigid=True,
        display=False
    )

    # Store and Apply the affine to the blocks
    block_objects[0].transforms['stacking']['affine'] = A.tolist()
    block_objects[0].transforms['stacking']['translation'] = T.tolist()
    block_objects[0].apply_stacking_()

    # Write out the dictionary
    IO.writedict(block_objects[0].transforms,
                 f'{out_dir}{block_objects[0].name}/{block_objects[0].name}_rigid_config.yaml')

    for src_block, tar_block in zip(block_objects[1:], block_objects[:-1]):
        #
        # if tar_block.name == 'block05':
        #     comb_verts, comb_faces = IO.combine_arbitrary(
        #         block_objects[6].surface_dict['support']['vertices'],
        #         tar_block.surface_dict['head']['vertices'],
        #         block_objects[6].surface_dict['support']['faces'],
        #         tar_block.surface_dict['head']['faces'],
        #     )
        #     # Now make the 'foot' equal to the combined ones
        #     tar_block.surface_dict['head']['vertices'] = comb_verts
        #     tar_block.surface_dict['head']['faces'] = comb_faces
        #
        #     # We need to make sure the normals and centers are updates as well
        #     tar_block._calc_normals()
        #     tar_block._calc_centers()
        #     tar_block._calc_means()

        A, T = currents.match(
            src_block.surface_dict['foot'],
            tar_block.surface_dict['head'],
            src_block.transforms['stacking'],
            init_affine=init_affine,
            rigid=True,
            display=True
        )

        # Store and Apply the affine to the blocks
        src_block.transforms['stacking']['affine'] = A.tolist()
        src_block.transforms['stacking']['translation'] = T.tolist()
        src_block.apply_stacking_()

        # Write out the object
        IO.writeobj(
            src_block.surface_dict['exterior']['vertices'],
            src_block.surface_dict['exterior']['faces'],
            f'{out_dir}{src_block.name}/exterior_rigid.obj'
        )

        # Write the dictionary
        IO.writedict(src_block.transforms, f'{out_dir}{src_block.name}/{src_block.name}_rigid_config.yaml')


def affine_exterior(exvivo_object, block_objects, out_dir):

    device = 'cuda:1'

    # Make sure the objects are on the GPU - This is not the same as before
    centers = torch.empty(0).cuda(device)
    exvivo_object.to_device_('decimate', device)
    for block_object in block_objects:
        block_object.to_device_('exterior', device)
        centers = torch.cat((centers, block_object.surface_dict['exterior']['centers']))

    exterior_surface_mean = centers.mean(0).to(device)

    # Define the parameters to be optimized
    affine = torch.tensor(np.eye(3)).float().to(device)
    affine.requires_grad = True
    translation = (exvivo_object.surface_dict['decimate']['mean'] - exterior_surface_mean).clone().float()
    translation = translation.to(device)
    translation.requires_grad = True

    # Create the loss function
    model = energy.CurrentsEnergy(
        exvivo_object.surface_dict['decimate']['normals'],
        exvivo_object.surface_dict['decimate']['centers'],
        block_objects[0].transforms['exvivo']['sigma'],
        kernel='cauchy'
    )

    # Create the optimizer
    optimizer = optim.SGD([
        {'params': affine, 'lr': block_objects[0].transforms['exvivo']['lr_affine']},
        {'params': translation, 'lr': block_objects[0].transforms['exvivo']['lr_translation']}],
        momentum=0.9, nesterov=True)

    for epoch in range(0, block_objects[0].transforms['exvivo']['n_iter']//5):
        loss = 0
        optimizer.zero_grad()
        for block_object in block_objects:
            loss += model(
                block_object.surface_dict['exterior']['normals'],
                block_object.surface_dict['exterior']['centers'],
                exvivo_object.surface_dict['decimate']['normals'],
                exvivo_object.surface_dict['decimate']['centers'],
                affine,
                translation,
                block_object.transforms['exvivo']['sigma'],
                exterior_surface_mean
            )

        loss.backward()  # Compute the gradients
        optimizer.step()  # Accumulates the gradients in optimizer

        print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')

    for epoch in range(0, block_objects[0].transforms['exvivo']['n_iter']//5):
        loss = 0
        optimizer.zero_grad()
        for block_object in block_objects:
            loss += model(
                block_object.surface_dict['exterior']['normals'],
                block_object.surface_dict['exterior']['centers'],
                exvivo_object.surface_dict['decimate']['normals'],
                exvivo_object.surface_dict['decimate']['centers'],
                affine,
                translation,
                block_object.transforms['exvivo']['sigma'] / 10,
                exterior_surface_mean
            )

        loss.backward()  # Compute the gradients
        optimizer.step()  # Accumulates the gradients in optimizer

        print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')

    # Update the transforms for the source block
    affine = affine.detach().cpu().numpy()
    # Need to update the translation to account for not rotation about the origin
    translation = translation.detach().cpu().numpy()
    translation = -np.matmul(affine, exterior_surface_mean.cpu().numpy()) + \
                  exterior_surface_mean.cpu().numpy() + translation

    for block_object in block_objects:
        # Store and Apply the affine to the blocks
        block_object.transforms['exvivo']['affine'] = affine.tolist()
        block_object.transforms['exvivo']['translation'] = translation.tolist()

        # Make sure the stacking affine has been applied
        if not block_object.transforms['stacking']['applied']:
            block_object.apply_stacking_()

        block_object.apply_exvivo_()
        IO.writedict(block_object.transforms, f'{out_dir}{block_object.name}/{block_object.name}_rigid_config.yaml')


def refine_stack(exvivo, block_objects, device, out_dir):

    ### Temporary ###
    for block in block_objects:
        block.transforms['refine']['sigma_exterior'] = 5.0
        block.transforms['refine']['sigma_blocks'] = 0.5
        block.transforms['refine']['lr_affine'] = 0.0000005
        block.transforms['refine']['lr_translation'] = 0.0000002
        block.transforms['refine']['n_iter'] = 200

    # Make sure all of the surfaces are on the GPU
    for i, block in enumerate(block_objects, 1):
        block.to_device_('exterior', device)
        if i > 1:
            block.to_device_('head', device)
            block.flip_normals_('head')
        if i < len(block_objects):
            block.to_device_('foot', device)
    exvivo.to_device_('decimate', device)

    # # For some reason the exvivo is flipped
    # exvivo.flip_normals_('decimate')

    # Define the parameters to be optimized
    affine = torch.eye(3, dtype=torch.float, device=device, requires_grad=False).repeat(len(block_objects), 1, 1)
    affine.requires_grad = True  # Have to do this after the repeat
    translation = torch.zeros((len(block_objects), 1, 3), dtype=torch.float, device=device, requires_grad=True)

    # Create the loss function
    exteriorModel = energy.CurrentsEnergy(
        exvivo.surface_dict['decimate']['normals'],
        exvivo.surface_dict['decimate']['centers'],
        block_objects[0].transforms['refine']['sigma_exterior'],
        kernel='cauchy'
    )
    blockModel = energy.CurrentsEnergy(
        torch.cat([x.surface_dict['head']['normals'] for x in block_objects[1:]], 0),
        torch.cat([x.surface_dict['head']['centers'] for x in block_objects[1:]], 0),
        block_objects[0].transforms['refine']['sigma_blocks'],
        kernel='cauchy'
    )

    # Create the optimizer
    optimizer = optim.SGD([
        {'params': affine, 'lr': block_objects[0].transforms['refine']['lr_affine']},
        {'params': translation, 'lr': block_objects[0].transforms['refine']['lr_translation']}],
        momentum=0.9, nesterov=True)

    se = 1.0
    sf = 1.0

    for epoch in range(0, block_objects[0].transforms['refine']['n_iter']):
        loss = 0
        optimizer.zero_grad()

        # Calculate the energy of each block with the exvivo
        for b, block in enumerate(block_objects, 0):
            loss += se * exteriorModel(
                block.surface_dict['exterior']['normals'],
                block.surface_dict['exterior']['centers'],
                exvivo.surface_dict['decimate']['normals'],
                exvivo.surface_dict['decimate']['centers'],
                affine[b],
                translation[b],
                block.transforms['refine']['sigma_exterior'],
                block.mesh_mean
            )

        # Calculate the energy of the faces with the appropriate neighbors
        loss += sf * blockModel(
                block_objects[0].surface_dict['foot']['normals'],
                block_objects[0].surface_dict['foot']['centers'],
                block_objects[1].surface_dict['head']['normals'],
                block_objects[1].surface_dict['head']['centers'],
                affine[0],
                translation[0],
                block_objects[0].transforms['refine']['sigma_blocks'],
                block_objects[0].mesh_mean
            )

        for i, (front, middle, back) in enumerate(zip(block_objects[:-2], block_objects[1:-1], block_objects[2:]), 1):
            #
            # # Don't union the surfaces, but calculate them separately so there is no overlap with large sigma
            loss += sf * blockModel(
                middle.surface_dict['head']['normals'],
                middle.surface_dict['head']['centers'],
                front.surface_dict['foot']['normals'],
                front.surface_dict['foot']['centers'],
                affine[i],
                translation[i],
                block_objects[i].transforms['refine']['sigma_blocks'],
                block_objects[i].mesh_mean
            )

            loss += sf * blockModel(
                middle.surface_dict['foot']['normals'],
                middle.surface_dict['foot']['centers'],
                back.surface_dict['head']['normals'],
                back.surface_dict['head']['centers'],
                affine[i],
                translation[i],
                block_objects[i].transforms['refine']['sigma_blocks'],
                block_objects[i].mesh_mean
            )

        loss += sf * blockModel(
            block_objects[-1].surface_dict['head']['normals'],
            block_objects[-1].surface_dict['head']['centers'],
            block_objects[-2].surface_dict['foot']['normals'],
            block_objects[-2].surface_dict['foot']['centers'],
            affine[-1],
            translation[-1],
            block_objects[-1].transforms['refine']['sigma_blocks'],
            block_objects[-1].mesh_mean
        )

        loss.backward()
        optimizer.step()
        print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')
    #
    # for epoch in range(0, block_objects[0].transforms['refine']['n_iter'] // 2):
    #     loss = 0
    #     optimizer.zero_grad()
    #
    #     # Calculate the energy of each block with the exvivo
    #     for b, block in enumerate(block_objects, 0):
    #         loss += se * exteriorModel(
    #             block.surface_dict['exterior']['normals'],
    #             block.surface_dict['exterior']['centers'],
    #             exvivo.surface_dict['decimate']['normals'],
    #             exvivo.surface_dict['decimate']['centers'],
    #             affine[b],
    #             translation[b],
    #             block.transforms['refine']['sigma_exterior'] / 10,
    #             block.mesh_mean
    #         )
    #
    #     # Calculate the energy of the faces with the appropriate neighbors
    #     loss += sf * blockModel(
    #         block_objects[0].surface_dict['foot']['normals'],
    #         block_objects[0].surface_dict['foot']['centers'],
    #         block_objects[1].surface_dict['head']['normals'],
    #         block_objects[1].surface_dict['head']['centers'],
    #         affine[0],
    #         translation[0],
    #         block_objects[0].transforms['refine']['sigma_blocks'],
    #         block_objects[0].mesh_mean
    #     )
    #
    #     for i, (front, middle, back) in enumerate(zip(block_objects[:-2], block_objects[1:-1], block_objects[2:]),
    #                                               1):
    #         #
    #         # # Don't union the surfaces, but calculate them separately so there is no overlap with large sigma
    #         loss += sf * blockModel(
    #             middle.surface_dict['head']['normals'],
    #             middle.surface_dict['head']['centers'],
    #             front.surface_dict['foot']['normals'],
    #             front.surface_dict['foot']['centers'],
    #             affine[i],
    #             translation[i],
    #             block_objects[i].transforms['refine']['sigma_blocks'],
    #             block_objects[i].mesh_mean
    #         )
    #
    #         loss += sf * blockModel(
    #             middle.surface_dict['foot']['normals'],
    #             middle.surface_dict['foot']['centers'],
    #             back.surface_dict['head']['normals'],
    #             back.surface_dict['head']['centers'],
    #             affine[i],
    #             translation[i],
    #             block_objects[i].transforms['refine']['sigma_blocks'],
    #             block_objects[i].mesh_mean
    #         )
    #
    #     loss += sf * blockModel(
    #         block_objects[-1].surface_dict['head']['normals'],
    #         block_objects[-1].surface_dict['head']['centers'],
    #         block_objects[-2].surface_dict['foot']['normals'],
    #         block_objects[-2].surface_dict['foot']['centers'],
    #         affine[-1],
    #         translation[-1],
    #         block_objects[-1].transforms['refine']['sigma_blocks'],
    #         block_objects[-1].mesh_mean
    #     )
    #
    #     loss.backward()
    #     optimizer.step()
    #
    #     print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')

    return affine, translation

    # for i, block in enumerate(block_objects, 0):
    #     # Update the transforms for the source block
    #     affine_block = affine[i].clone().detach().cpu().numpy()
    #     # Need to update the translation to account for not rotation about the origin
    #     translation_block = translation[i].clone().detach().cpu().numpy()
    #     translation_block = -np.matmul(affine_block, block.mesh_mean.cpu().numpy()) + \
    #                                    block.mesh_mean.cpu().numpy() + translation_block
    #
    #     block.transforms['refine']['affine'] = affine_block.tolist()
    #     block.transforms['refine']['translation'] = translation_block.tolist()
    #     IO.writedict(block.transforms, f'{out_dir}{block.name}/{block.name}_rigid_config.yaml')
    #     block.apply_refine_()


def new_refine(exvivo, block_objects, device, out_dir):

    ### Temporary ###
    for block in block_objects:
        block.transforms['refine']['sigma'] = 2.0
        block.transforms['refine']['sigma_exterior'] = 1.0
        block.transforms['refine']['sigma_blocks'] = 5.0
        block.transforms['refine']['lr_affine'] = 0.0000002
        block.transforms['refine']['lr_translation'] = 0.0000002
        block.transforms['refine']['n_iter'] = 250
        if block.name != 'block12':
            block.flip_normals_('foot')

    # See what happens with a rigid match of block 12 and the exvivo with block 11 surface
    A, T = currents.match(
        [block_objects[0].surface_dict['exterior'], block_objects[0].surface_dict['head']],
        [exvivo.surface_dict['decimate'], block_objects[1].surface_dict['foot']],
        block_objects[0].transforms['stacking'],
        rigid=False,
        display=False,
        translation=torch.zeros(3).float()
    )

    block_objects[0].transforms['refine']['affine'] = A.tolist()
    block_objects[0].transforms['refine']['translation'] = T.tolist()
    block_objects[0].apply_refine_()

    # Write out the object
    IO.writeobj(
        block_objects[0].surface_dict['exterior']['vertices'],
        block_objects[0].surface_dict['exterior']['faces'],
        f'{out_dir}{block_objects[0].name}/test_exterior_rigid.obj'
    )

    scale_head = 1.0
    scale_foot = 1.0
    scale_exterior = 1.0

    # Move exvivo to device
    exvivo.surface_dict['decimate']['normals'] = exvivo.surface_dict['decimate']['normals'].to(device)
    exvivo.surface_dict['decimate']['centers'] = exvivo.surface_dict['decimate']['centers'].to(device)
    # exvivo.surface_dict['decimate']['mean'] = exvivo.surface_dict['decimate']['centers'].to(device)

    for i, (front, middle, back) in enumerate(zip(block_objects[:-2], block_objects[1:-1], block_objects[2:]), 1):

        # Make sure the objects are on the GPU - This is not the same as before
        front.to_device_('head', device)
        middle.to_device_('head', device)
        middle.to_device_('foot', device)
        middle.to_device_('exterior', device)
        back.to_device_('foot', device)

        # Define the parameters to be optimized
        affine = torch.tensor(np.eye(3)).float()
        affine = affine.to(device).float()
        affine.requires_grad = True

        translation = torch.zeros(3).float()
        translation = translation.to(device).float()
        translation.requires_grad = True

        # Create the optimizer
        optimizer = optim.SGD([
            {'params': affine, 'lr': block_objects[0].transforms['refine']['lr_affine']},
            {'params': translation, 'lr': block_objects[0].transforms['refine']['lr_translation']}],
            momentum=0.9, nesterov=True)

        face_list = [front.surface_dict['head'], back.surface_dict['foot']]

        # Create the loss function
        exteriorModel = energy.CurrentsEnergy(
            exvivo.surface_dict['decimate']['normals'],
            exvivo.surface_dict['decimate']['centers'],
            middle.transforms['refine']['sigma_exterior'],
            kernel='cauchy'
        )

        blockModel = energy.CurrentsEnergy(
            torch.cat([x['normals'] for x in face_list], 0),
            torch.cat([x['centers'] for x in face_list], 0),
            block_objects[0].transforms['refine']['sigma_blocks'],
            kernel='cauchy'
        )

        for epoch in range(0, middle.transforms['refine']['n_iter']):
            loss = 0
            optimizer.zero_grad()
            #
            # Match faces
            loss += scale_foot * blockModel(
                middle.surface_dict['head']['normals'],
                middle.surface_dict['head']['centers'],
                back.surface_dict['foot']['normals'],
                back.surface_dict['foot']['centers'],
                affine,
                translation,
                middle.transforms['refine']['sigma_blocks'],
                middle.mesh_mean
            )

            loss += scale_head * blockModel(
                middle.surface_dict['foot']['normals'],
                middle.surface_dict['foot']['centers'],
                front.surface_dict['head']['normals'],
                front.surface_dict['head']['centers'],
                affine,
                translation,
                middle.transforms['refine']['sigma_blocks'],
                middle.mesh_mean
            )

            # loss += scale_exterior * exteriorModel(
            #     middle.surface_dict['exterior']['normals'],
            #     middle.surface_dict['exterior']['centers'],
            #     exvivo.surface_dict['decimate']['normals'],
            #     exvivo.surface_dict['decimate']['centers'],
            #     affine,
            #     translation,
            #     middle.transforms['refine']['sigma_exterior'],
            #     middle.mesh_mean
            # )

            # with torch.no_grad():
            #     U, _, V = affine.clone().svd()
            #     affine.data = torch.mm(U, V.transpose(1, 0))

            loss.backward()
            optimizer.step()
            print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')

        # Now write things out
        middle.transforms['refine']['affine'] = affine.tolist()
        middle.transforms['refine']['translation'] = translation.tolist()
        middle.apply_refine_()

        # Write out the object
        IO.writeobj(
            middle.surface_dict['exterior']['vertices'],
            middle.surface_dict['exterior']['faces'],
            f'{out_dir}{middle.name}/test_exterior_rigid.obj'
        )

        del affine, translation


def main(args):

    rigid = True
    exvivo = False
    refine = False

    # Define some of the main directories
    raw_dir = f'{args.directory}{args.rabbit}/surfaces/raw/'
    rigid_dir = f'{args.directory}{args.rabbit}/surfaces/rigid/'

    block_dirs = sorted(glob.glob(f'{raw_dir}*'))
    # Get a list of the blocks
    blocks = [x.split('/')[-1] for x in block_dirs]

    # Make a directory for the rigid stacked
    if not os.path.exists(rigid_dir):
        os.makedirs(rigid_dir)

    # Generate Mesh objects for each of the blocks
    block_objects = []
    for block, block_dir in zip(blocks, block_dirs):

        # Check to see if a transform file exists and if not, make one
        transform = f'{rigid_dir}/{block}/{block}_rigid_config.yaml'
        if not os.path.exists(transform):
            os.makedirs(os.path.dirname(transform))
            config = init_config(transform, block)
            IO.writedict(config, transform)

        # Get a list of all the objects in the raw directory
        objects = sorted(glob.glob(block_dir + '/*.obj'))
        # Define the object extensions that we need for the registration
        exts = ['_head.obj', '_foot.obj', '_exterior.obj', '_support.obj']
        # Clean the list
        subsets = [x for x in objects if any(ext in x for ext in exts)]
        block_dict = dict()
        for subset in subsets:
            block_dict[os.path.splitext(subset.split('_')[-1])[0]] = subset

        block_objects.append(currents.MeshObject(block_dict, block, transform_file=transform))

    # Generate the Mesh object for the exvivio
    objects = sorted(glob.glob(args.exvivo + '/*.obj'))
    exts = ['_block', '_decimate.obj']
    subsets = [x for x in objects if any(ext in x for ext in exts) and 'ablation' not in x]
    exvivo_dict = dict()
    for subset in subsets:
        exvivo_dict[os.path.splitext(subset.split('_')[-1])[0]] = subset
    exvivo_object = currents.MeshObject(exvivo_dict)

    # Flip the normals of the exvivo
    exvivo_object.flip_normals_('decimate')

    # Because of how the faces are generated, we need to flip one of the surfaces
    for i, block in enumerate(block_objects, 1):
        block.flip_normals_('exterior')
        if i < 12:
            block.flip_normals_('foot')

    if rigid:
        rigid_stack(block_objects[::-1], exvivo_object, rigid_dir)
    else:
        for block_object in block_objects:
            if not block_object.transforms['stacking']['applied']:
                block_object.apply_stacking_()

    exterior_verts, exterior_faces = IO.combine_surfaces(block_objects, 'exterior')
    IO.writeobj(exterior_verts.numpy(), exterior_faces.numpy(), f'{rigid_dir}rigid_exterior_comb.obj')

    if exvivo:
        # Solve for an affine with the exvivo
        affine_exterior(exvivo_object, block_objects, rigid_dir)
    else:
        for block_object in block_objects:
            if not block_object.transforms['exvivo']['applied']:
                block_object.apply_exvivo_()

    # Now wrtie out the affine applied exteriors
    exterior_verts, exterior_faces = IO.combine_surfaces(block_objects, 'exterior')
    IO.writeobj(exterior_verts.numpy(), exterior_faces.numpy(), f'{rigid_dir}affine_exterior_comb.obj')

    # Refine the blocks
    if refine:
        new_refine(exvivo_object, block_objects[::-1], 'cuda:1', rigid_dir)
    else:
        for block_object in block_objects:
            if not block_object.transforms['refine']['applied']:
                block_object.apply_refine_()

    # affine, translation = refine_stack(exvivo_object, block_objects, 'cuda:1', rigid_dir)
    #
    # for i, block in enumerate(block_objects[::-1]):
    #     block._apply_affine_(affine[i], translation[i])

    # Now wrtie out the affine applied exteriors
    exterior_verts, exterior_faces = IO.combine_surfaces(block_objects, 'exterior')
    IO.writeobj(exterior_verts.numpy(), exterior_faces.numpy(), f'{rigid_dir}refine_exterior_comb.obj')

    # Register the invivo and exvivo
    invivo_affine, invivo_translation = exvivo_invivo.main(
        '/home/sci/blakez/ucair/18_047/rawVolumes/PostImaging_2018-07-02/',
        '/home/sci/blakez/ucair/18_047/rawVolumes/ExVivo_2018-07-26/'
    )
    generate_transforms.main(block_objects, invivo_affine, invivo_translation, rigid_dir)


if __name__ == '__main__':
    main(args)
