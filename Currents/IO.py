import yaml
import torch
import numpy as np

from Currents import funcs


def writedict(data, file):
    with open(file, 'w') as f:
        yaml.dump(data, f, default_flow_style=False)
        f.close()


def readdict(file):
    with open(file, 'r') as f:
        data = yaml.load(f)
    f.close()
    return data


def readobj(file, device='cpu'):
    with open(file) as f:
        lines = f.readlines()
        verts = np.array([list(map(float, line.split()[1:4])) for line in lines if line.startswith('v ')])
        faces = np.array([list(map(int, line.split()[1:4])) for line in lines if line.startswith('f ')])
        # Subtract 1 because the faces are 1 indexed and need to be 0 indexed for python
        f.close()
        faces -= 1

        verts = torch.tensor(verts, dtype=torch.float, device=device, requires_grad=False)
        faces = torch.tensor(faces, dtype=torch.long, device=device, requires_grad=False)

    return dict(vertices=verts, faces=faces)


def writeobj(vert, faces, file):
    with open(file, 'w') as f:
        f.write("# OBJ file\n")
        for v in vert.tolist():
            f.write("v")
            for i in range(0, len(v)):
                f.write(" %.4f" % (v[i]))
            f.write("\n")
        for p in faces:
            f.write("f")
            for i in range(0, len(p)):
                f.write(" %d" % (p[i] + 1))
            f.write("\n")


def combine_arbitrary(verts_one, verts_two, faces_one, faces_two):
    length_one = len(verts_one)
    updated_faces = faces_two + length_one
    comb_vertices = torch.cat((verts_one, verts_two), 0)
    comb_faces = torch.cat((faces_one, updated_faces), 0)

    return comb_vertices, comb_faces


def combine_surf(surface1, surface2):
    length = len(surface1['vertices'])
    updated_faces = surface1['faces'] + length
    comb_vertices = torch.cat((surface1['vertices'], surface2['vertices']), 0)
    comb_faces = torch.cat((surface1['faces'], updated_faces), 0)
    comb_normals = torch.cat((surface1['normals'], surface2['normals']), 0)
    comb_centers = torch.cat((surface1['centers'], surface2['centers']), 0)

    return comb_vertices, comb_faces, comb_normals, comb_centers


def join_surfaces(surface_list):
    length_one = len(verts_one)
    updated_faces = faces_two + length_one
    comb_vertices = torch.cat((verts_one, verts_two), 0)
    comb_faces = torch.cat((faces_one, updated_faces), 0)

    return comb_vertices, comb_faces


def combine_surfaces(surface_list, subset):
    comb_faces = surface_list[0].surface_dict[subset]['faces']
    comb_vertices = surface_list[0].surface_dict[subset]['vertices']
    for surface in surface_list[1:]:
        current_length = len(comb_vertices)
        updated_faces = surface.surface_dict[subset]['faces'] + current_length
        comb_vertices = torch.cat((comb_vertices, surface.surface_dict[subset]['vertices']), 0)
        comb_faces = torch.cat((comb_faces, updated_faces), 0)

    return comb_vertices, comb_faces



