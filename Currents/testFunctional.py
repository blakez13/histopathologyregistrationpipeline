import os
import sys
import glob
import copy
import torch
import numpy as np
import matplotlib
import subprocess as sp

matplotlib.use('Qt5Agg')
# matplotlib.pyplot.ion()
from types import SimpleNamespace

import torch.optim as optim
from Currents import plot
from Currents import IO
from Currents import energy
from Currents import currents


def _calc_normals(in_dict):

    tris = in_dict['vertices'][in_dict['faces']]

    a = tris[:, 0, :]
    b = tris[:, 1, :]
    c = tris[:, 2, :]

    in_dict['normals'] = 0.5 * torch.cross((a - b), (c - b), dim=1)


def _calc_centers(in_dict):

    tris = in_dict['vertices'][in_dict['faces']]
    in_dict['centers'] = (1 / 3.0) * tris.sum(1)


# Load the ilfes
source = IO.readobj('/home/sci/blakez/FunctionalCurrentsTest/Block5_head.obj', 'cuda')
target = IO.readobj('/home/sci/blakez/FunctionalCurrentsTest/Block4_foot.obj', 'cuda')

_calc_normals(source)
_calc_centers(source)

_calc_normals(target)
_calc_centers(target)

# Flip the source normals
source['normals'] *= -1

# Define sigma
sigma = 5.0

# Plot some things
# Plot the surfaces
[_, fig, ax] = plot.plot_surface(
    target['vertices'],
    target['faces'],
)
[src_mesh, _, _] = plot.plot_surface(
    source['vertices'],
    source['faces'],
    fig=fig,
    ax=ax,
    color=[1, 0, 0]
)

# Create the loss function
model = energy.CurrentsEnergy(
    target['normals'],
    target['centers'],
    sigma=sigma,
    kernel='cauchy'
)

# Define the parameters to be optimized
affine = torch.from_numpy(np.eye(3)).float().to('cuda')
affine.requires_grad = True
translation = (target['centers'].mean(0) - source['centers'].mean(0)).clone().float()
translation = translation.to('cuda')
translation.requires_grad = True

# Create the optimizer
optimizer = optim.SGD([
    {'params': affine, 'lr': 0.000001},
    {'params': translation, 'lr': 0.0001}],
    momentum=0.9, nesterov=True)

for epoch in range(0, 100):

    optimizer.zero_grad()
    loss = model(
        source['normals'],
        source['centers'],
        None,
        target['normals'],
        target['centers'],
        None,
        affine,
        translation,
        sigma,
        source['centers'].mean(0)
    )

    print(f'===> Iteration {epoch:3} Energy: {loss.item():.3f}')

    loss.backward()  # Compute the gradients
    optimizer.step()  # Accumulates the gradients in optimizer
    #
    # # As we are stacking the blocks, make sure the transform is rigid
    # with torch.no_grad():
    #     U, _, V = affine.clone().svd()
    #     affine.data = torch.mm(U, V.transpose(1, 0))

    if epoch % 10 == 0:
        plot.update_plot(
            src_mesh,
            source['vertices'].cpu(),
            source['faces'].cpu(),
            affine,
            translation,
            source['centers'].mean(0).cpu()
        )
# Update the transforms for the source block
affine = affine.detach().cpu().numpy()
# Need to update the translation to account for not rotation about the origin
translation = translation.detach().cpu().numpy()
translation = -np.matmul(affine, source['centers'].mean(0).cpu().numpy()) + \
              source['centers'].mean(0).cpu().numpy() + translation
