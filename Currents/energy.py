import torch
import torch.nn as nn


class CurrentsEnergy(nn.Module):

    def __init__(self, tar_normals, tar_centers, sigma, kernel='cauchy'):
        super(CurrentsEnergy, self).__init__()

        if kernel == 'cauchy':
            self.kernel = self.cauchy

        elif kernel == 'gaussian':
            self.kernel = self.gaussian

        else:
            raise NotImplementedError

        self._calc_e3(tar_normals, tar_centers, sigma)

    def forward(self, src_normals, src_centers,tar_normals, tar_centers, affine, translation, sigma, src_mean):

        # Compute the scaling factor for the affine transformation
        ns = torch.det(affine) * torch.t(torch.inverse(affine))

        # Update the mean
        ut = src_mean + translation

        # Scale the source normals
        src_nrm_sc = torch.mm(ns, src_normals.permute(1, 0)).permute(1, 0)

        # Affine transform the source centers about the mean
        src_cnt_tf = torch.mm(affine, (src_centers - src_mean).permute(1, 0)).permute(1, 0) + ut

        # Calculate the self term
        e1 = torch.mul(torch.mm(src_nrm_sc, src_nrm_sc.permute(1, 0)),
                       self.kernel(self.distance(src_cnt_tf, src_cnt_tf), sigma)).sum()

        # Calculate the cross term
        e2 = torch.mul(torch.mm(tar_normals, src_nrm_sc.permute(1, 0)),
                       self.kernel(self.distance(src_cnt_tf, tar_centers), sigma)).sum()

        return e1 - 2 * e2 + self.e3

    def _calc_e3(self, tar_normals, tar_centers, sigma):
        self.e3 = torch.mul(torch.mm(tar_normals, tar_normals.permute(1, 0)),
                            self.kernel(self.distance(tar_centers, tar_centers), sigma)).sum()

    @staticmethod
    def distance(src_centers, tar_centers):
        return ((src_centers.permute(1, 0).unsqueeze(0) - tar_centers.unsqueeze(2)) ** 2).sum(1)

    @staticmethod
    def colordiff(src_colors, tar_colors):
        return ((src_colors.permute(1, 0).unsqueeze(0) - tar_colors.unsqueeze(2)) ** 2).sum(1)

    @staticmethod
    def gaussian(d, sigma):
        return (d / (-2 * (sigma ** 2))).exp()

    @staticmethod
    def cauchy(d, sigma):
        return 1 / (1 + (d / sigma)) ** 2


# class _SigmaScheduler(object):
#     def __init__(self, model, last_epoch=-1):
#         self.model = model
#         if last_epoch == -1:
#             for group in optimizer.param_groups:
#                 group.setdefault('initial_lr', group['lr'])
#             last_epoch = 0
#         else:
#             for i, group in enumerate(optimizer.param_groups):
#                 if 'initial_lr' not in group:
#                     raise KeyError("param 'initial_lr' is not specified "
#                                    "in param_groups[{}] when resuming an optimizer".format(i))
#         self.base_lrs = list(map(lambda group: group['initial_lr'], optimizer.param_groups))
#         self.step(last_epoch)
#
#     def state_dict(self):
#         """Returns the state of the scheduler as a :class:`dict`.
#         It contains an entry for every variable in self.__dict__ which
#         is not the optimizer.
#         """
#         return {key: value for key, value in self.__dict__.items() if key != 'optimizer'}
#
#     def load_state_dict(self, state_dict):
#         """Loads the schedulers state.
#         Arguments:
#             state_dict (dict): scheduler state. Should be an object returned
#                 from a call to :meth:`state_dict`.
#         """
#         self.__dict__.update(state_dict)
#
#     def get_lr(self):
#         raise NotImplementedError
#
#     def step(self, epoch=None):
#         if epoch is None:
#             epoch = self.last_epoch + 1
#         self.last_epoch = epoch
#         for param_group, lr in zip(self.optimizer.param_groups, self.get_lr()):
#             param_group['lr'] = lr
#
#
#
# class StepLR(_SigmaScheduler):
#     """Decays the learning rate of each parameter group by gamma every
#     step_size epochs. Notice that such decay can happen simultaneously with
#     other changes to the learning rate from outside this scheduler. When
#     last_epoch=-1, sets initial lr as lr.
#     Args:
#         optimizer (Optimizer): Wrapped optimizer.
#         step_size (int): Period of learning rate decay.
#         gamma (float): Multiplicative factor of learning rate decay.
#             Default: 0.1.
#         last_epoch (int): The index of last epoch. Default: -1.
#     Example:
#         >>> # Assuming optimizer uses lr = 0.05 for all groups
#         >>> # lr = 0.05     if epoch < 30
#         >>> # lr = 0.005    if 30 <= epoch < 60
#         >>> # lr = 0.0005   if 60 <= epoch < 90
#         >>> # ...
#         >>> scheduler = StepLR(optimizer, step_size=30, gamma=0.1)
#         >>> for epoch in range(100):
#         >>>     train(...)
#         >>>     validate(...)
#         >>>     scheduler.step()
#     """
#
#     def __init__(self, optimizer, step_size, gamma=0.1, last_epoch=-1):
#         self.step_size = step_size
#         self.gamma = gamma
#         super(StepLR, self).__init__(optimizer, last_epoch)
#
#     def get_lr(self):
#         if (self.last_epoch == 0) or (self.last_epoch % self.step_size != 0):
#             return [group['lr'] for group in self.optimizer.param_groups]
#         return [group['lr'] * self.gamma
#                 for group in self.optimizer.param_groups]