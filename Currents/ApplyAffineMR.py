from __future__ import print_function

import os
import sys
import glob
import pickle
import argparse
import numpy as np
import PyCA.Core as ca
import PyCAApps as apps
import RabbitCommon as rc
import PyCA.Common as common

import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend('Qt5Agg')

import PyCACalebExtras.Common as cc
# import PyCABlakeExtras.Common as cb

plt.ion()
plt.close('all')
cc.SelectGPU(1)


def apply_affine(im, affine):

    # Now solve for the grid and create a field and image with that grid
    aff_grid = rc.SolveAffineGrid(im.grid(), affine)

    aff_im = ca.Image3D(aff_grid, im.memType())
    h = ca.Field3D(aff_grid, im.memType())

    # Now make the inverse transform into an h field
    cc.AtoHReal(h, np.linalg.inv(affine))
    cc.ApplyHReal(aff_im, im, h, bg=3)

    return aff_im


parser = argparse.ArgumentParser(description='Affine Apply ')
parser.add_argument('-v', '--volume', type=str, help='Path to MR volume', required=True)
parser.add_argument('-a', '--affine', type=str, help='Affine Transform', required=True)
parser.add_argument('-o', '--output', type=str, help='Output file', required=True)
parser.add_argument('-s', '--slice', type=str, help='Mic slice to load', required=True)
parser.add_argument('-z', '--z_offset', type=str, help='amount of z offset for the slice', required=True)
parser.add_argument('-u', '--high', type=str, help='High res slice to load', required=True)
args = parser.parse_args()

mem_type = ca.MEM_DEVICE

slice_name = os.path.basename(os.path.splitext(args.slice)[0])

if not os.path.exists(args.output):
    # Load the block
    vol = common.LoadITKImage(args.volume, mem_type)
    affine = np.array([float(x) for x in args.affine.split(' ')[1:]]).reshape(4, 4)
    # Apply the affine to the block
    aff_block = apply_affine(vol, affine)
    common.SaveITKImage(aff_block, args.output)

if not os.path.exists(os.path.dirname(args.slice) + '/../mr_to_{0}.nii.gz'.format(slice_name)):
    aff_block = common.LoadITKImage(args.output, ca.MEM_DEVICE)

    mic_slice = common.LoadITKImage(args.slice, ca.MEM_DEVICE)

    # Need to make sure that the slice has the right amount of Z-offset
    mic_slice.setOrigin(ca.Vec3Df(mic_slice.origin().x, mic_slice.origin().y, float(args.z_offset)))

    mr_resample = mic_slice.copy()
    cc.ResampleWorld(mr_resample, aff_block, bg=3)

    # Now we need to apply the 2D affine

    high_col = common.LoadITKImage(args.high)
    high_slice = cc.ExtractSlice(high_col, sliceIdx=0, dim='x')
    high_slice = cc.SwapAxes(high_slice, 0, 1)
    high_slice = cc.SwapAxes(high_slice, 1, 2)

    grid = cc.MakeGrid(high_slice.size(), origin='center')
    high_slice.setGrid(grid)

    mic_slice.toType(ca.MEM_HOST)
    #
    landmarks = rc.LandmarkPicker([np.squeeze(mic_slice.asnp()), np.squeeze(high_slice.asnp())])
    for lm in landmarks:
        lm[0] = np.ndarray.tolist(np.multiply(lm[0], mic_slice.spacing().tolist()[0:2]) + mic_slice.origin().tolist()[0:2])
        lm[1] = np.ndarray.tolist(np.multiply(lm[1], high_slice.spacing().tolist()[0:2]) + high_slice.origin().tolist()[0:2])
    aff = apps.SolveAffine(landmarks)

    mic_slice.toType(ca.MEM_DEVICE)
    high_slice.toType(ca.MEM_DEVICE)

    i_out = high_slice.copy()

    i_out.setOrigin(ca.Vec3Df(i_out.origin().x, i_out.origin().y, mr_resample.origin().z))

    cc.ApplyAffineReal(i_out, mr_resample, aff)

    # Save the resampled image
    common.SaveITKImage(i_out, os.path.dirname(args.slice) + '/../mr_to_{0}.nii.gz'.format(slice_name))
