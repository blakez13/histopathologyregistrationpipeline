import os
import sys
import glob
import copy
import torch
import argparse
import numpy as np
import subprocess as sp

from PIL import Image
Image.MAX_IMAGE_PIXELS = 100000000000

import SimpleITK as sitk
import matplotlib
matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt
plt.ion()

# import RabbitCommon as rc

from collections import OrderedDict
import torch.optim as optim
from Currents import plot
from Currents import IO
from Currents import energy
from Currents import currents
from Currents import exvivo_invivo
from Currents import generate_transforms

def get_identity(shape):
    """Assuming z, x, y"""
    vecs = [torch.linspace(-1, 1, shape[x]) for x in range(0, len(shape))]
    grids = torch.meshgrid(vecs)
    grids = [grid.unsqueeze(-1) for grid in grids]

    # Need to flip the x and y dimension as per affine grid
    grids[-2], grids[-1] = grids[-1], grids[-2]

    return torch.cat(grids, -1)


def read_mhd_header(filename):

    with open(filename, 'r') as in_mhd:
        long_string = in_mhd.read()

    short_strings = long_string.split('\n')
    key_list = [x.split(' = ')[0] for x in short_strings[:-1]]
    value_list = [x.split(' = ')[1] for x in short_strings[:-1]]
    a = OrderedDict(zip(key_list, value_list))

    return a


def write_mhd_header(filename, dictionary):
    long_string = '\n'.join(['{0} = {1}'.format(k, v) for k, v in dictionary.iteritems()])
    with open(filename, 'w+') as out:
        out.write(long_string)


def LandmarkPicker(imList):
    '''Allows the user to select landmark correspondences between any number of images. The images must be in a list and must be formatted as numpy arrays. '''

    class PointPicker(object):
        '''Image class for picking landmarks'''

        def __init__(self, X):
            self.fig = plt.figure()
            self.ax = self.fig.gca()
            self.im = self.ax.imshow(X, cmap='gray')
            self.shape = np.shape(X)
            self.cords = []
            self.fig.canvas.mpl_connect('button_press_event', self.onclick)

        def onclick(self, event):
            if event.button == 3:
                xidx = int(round(event.xdata))
                yidx = int(round(event.ydata))
                self.cords.append([xidx, yidx])
                self.plot()

        def plot(self):
            self.ax.scatter(self.cords[-1][0], self.cords[-1][1])
            self.ax.set_xlim([0, self.shape[1]])
            self.ax.set_ylim([self.shape[0], 0])
            plt.pause(0.001)

    plt.ion()
    pickerList = [PointPicker(im) for im in imList]
    plt.show()
    # while all([p.fig.get_visible() for p in pickerList]):
    Done = False
    while not Done:
        plt.pause(0.1)
        Done = plt.waitforbuttonpress()

    lmCoords = [p.cords for p in pickerList]
    lengths = [len(l) for l in lmCoords]
    if min(lengths) != max(lengths):
        raise Exception('Lists of landmarks were not consistent for each image, start over!')

    for p in pickerList:
        plt.close(p.fig)

    landmarks = np.array(lmCoords).swapaxes(0, 1).tolist()

    if len(pickerList) == 1:
        return landmarks

    for lm in landmarks:
        lm[0] = lm[0][::-1]
        lm[1] = lm[1][::-1]

    return landmarks


def SolveAffine(landmarks):
    ''' given a set of N (2D or 3D) real correspondences, i.e. :
    landmarks = [ [[x0, x1, x2], [y0, y1, y2]],
                  ...
                  [[x0, x1, x2], [y0, y1, y2]] ]

    returns an affine transformation where for all landmark pairs,

    A*[x0, x1, x2, 1] \approx [y0, y1, y2, 1]

    If given 3D corresponding points, A is a 4x4 matrix
    If given 2D corresponding points, A is a 3x3 matrix
    '''

    dim = len(landmarks[0][0])
    dim1 = dim+1

    n = len(landmarks)              # number of landmarks
    Msub = np.ones((n, dim1))
    b = np.ones(n*dim)
    for i in range(n):
        Msub[i, 0:dim] = landmarks[i][1][:]
        b[i] = landmarks[i][0][0]
        b[i+n] = landmarks[i][0][1]
        if dim > 2:
            b[i+2*n] = landmarks[i][0][2]
    M = np.zeros((n*dim, dim1*dim))
    M[0:n, 0:dim1] = Msub
    M[n:2*n, dim1:2*dim1] = Msub
    if dim > 2:
        M[2*n:3*n, 2*dim1:3*dim1] = Msub

    if n == dim+1:
        z = np.linalg.solve(M, b)
    elif n > dim+1:
        z = np.linalg.lstsq(M, b, rcond=-1)
        z = z[0]
    else:
        print("Not enough landmarks!")
        return

    Ainv = np.zeros((dim1, dim1))
    Ainv[0][:] = z[0:dim1]
    Ainv[1][:] = z[dim1:2*dim1]
    if dim > 2:
        Ainv[2][:] = z[2*dim1:3*dim1]
    Ainv[dim][dim] = 1
    A = np.linalg.inv(Ainv)

    return A


def main(args):

    tform_dir = '/hdscratch/ucair/blockface/18_047/surfaces/rigid/'

    # Need to load the affine
    tform_file = f'{tform_dir}{args.block}/{args.block}_to_invivo_paraview.txt'

    with open(tform_file, 'r') as f:
        to_invivo = np.array(list(map(float, [x.rstrip() for x in f.readlines()]))).reshape(4, 4)

    # Invert the affine
    affine = np.linalg.inv(to_invivo)
    affine_string = ''

    slice_list = sorted(glob.glob(f'{args.directory}registered/{args.block}/images/*.mhd'))
    slice_file = [x for x in slice_list if args.slice in x][0]
    # offset_index = slice_list.index(slice_file)

    raw_list = sorted(glob.glob(f'{args.directory}raw/{args.block}/*image.tif'))
    raw_file = [x for x in raw_list if args.slice in x][0]

    mhd_file = f'{args.directory}registered/{args.block}/{args.block}_histopathology_affine_volume.mhd'
    mhd_dict = read_mhd_header(mhd_file)

    # Load the blockface image
    block_list = sorted(glob.glob(f'{args.directory}../../blockface/18_047/microscopic_reference/{args.block}/images/*.mhd'))
    block_file = [x for x in block_list if args.slice in x][0]

    # origin = list(map(float, mhd_dict['Offset'].split(' ')))
    # Get the z offset from the blender file
    # with open(f'{tform_dir}blender_z_offsets.txt', 'r') as f:
    #     z_dict = eval(f.read())
    origin = list(map(float, mhd_dict['Offset'].split(' ')))
    spacing = list(map(float, mhd_dict['ElementSpacing'].split(' ')))
    z_offset = ((float(args.slice) - 1) * 0.05)

    # Need to make sure that there is a resampled MR Slice
    # mr_file = f'{args.invivo}008_----_t2_spc_1mm_iso_cor.nii.gz'
    # mr_file = f'{args.invivo}012_----_3D_VIBE_0.5x0.5x1_NoGrappa_3avg_fatsat_cor.nii.gz'
    mr_file = '/home/sci/blakez/motion_corrected_18_047_day3_CE.nii.gz'
    output_file = f'{args.directory}registered/{args.block}/MR_to_{args.block}.nii.gz'

    high_path = f'{os.path.dirname(raw_file)}/../{args.block}_{os.path.splitext(os.path.basename(raw_file))[0]}.mhd'
    if not os.path.exists(high_path):
        # Load the full resolution image
        high_res = Image.open(raw_file)
        np_high = np.array(high_res)
        ds_high = np_high[::4, ::4, :]

        itk_high = sitk.GetImageFromArray(ds_high)

        # Save the high res
        sitk.WriteImage(itk_high, high_path)

    p = sp.Popen(["/usr/bin/python",
                  "-u",
                  "/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/Currents/ApplyAffineMR.py",
                  "-v", f"{mr_file}",
                  "-a", f"{affine_string.join([f' {x}' for x in affine.flatten()])}",
                  "-o", f"{output_file}",
                  "-s", f"{slice_file}",
                  "-z", f"{z_offset}",
                  "-u", f"{high_path}"],
                 stdout=sp.PIPE)
    sout, _ = p.communicate()
    p.wait()

    print(sout.decode('utf-8'))
    sys.stdout.flush()

    slice_name = os.path.basename(os.path.splitext(slice_file)[0])

    # Load the slice that we are registering to
    mic_slice = sitk.ReadImage(slice_file)
    mri_slice = sitk.ReadImage(f'{os.path.dirname(slice_file)}/../mr_to_{slice_name}.nii.gz')
    hgh_slice = sitk.ReadImage(high_path)

    mic_array = sitk.GetArrayFromImage(mic_slice).squeeze()
    mri_array = sitk.GetArrayFromImage(mri_slice).squeeze()
    hgh_array = sitk.GetArrayFromImage(hgh_slice).squeeze()

    plt.imshow(hgh_array, interpolation='nearest')
    # plt.imshow(masked, cmap='gray', interpolation='none')
    plt.title('Microscopic Slice')

    plt.figure()
    plt.imshow(mri_array, cmap='gray', interpolation='nearest')
    plt.title('MR registered slice')

    # window = [650, 3300, 1400, 4800]  # Block 8 41
    window = [650, 3700, 1700, 5200]
    window_shape = mri_array[window[0]:window[1], window[2]:window[3]].shape

    plt.figure()
    plt.imshow(hgh_array[window[0]:window[1], window[2]:window[3], :], interpolation='nearest')
    # plt.imshow(masked, cmap='gray', interpolation='none')
    plt.title('Microscopic Slice')

    plt.figure()
    plt.imshow(mri_array[window[0]:window[1], window[2]:window[3]], cmap='gray', interpolation='nearest')
    plt.title('MR registered slice')

    mic_pil = Image.fromarray(hgh_array[window[0]:window[1], window[2]:window[3], :])
    mic_pil.save(f'/home/sci/blakez/ISTU_2019/{args.block}_{args.slice}_microscopic.png')

    mri_array = ((mri_array - mri_array.min()) / (mri_array.max() - mri_array.min()) * 255)
    mri_array = mri_array.astype('uint8')
    mri_pil = Image.fromarray(mri_array[window[0]:window[1], window[2]:window[3]])
    mri_pil.save(f'/home/sci/blakez/ISTU_2019/{args.block}_{args.slice}_mri.png')

    line_start = np.array([1270, 3040])
    line_stop = np.array([0, 2181])

    line_vector = line_stop - line_start
    line_vector = [-1 * line_vector[1], line_vector[0]]

    mask = np.zeros(window_shape)
    # mask[450, 0] = 1
    # mask[99, 559] = 1

    for x in range(0, window_shape[0]):
        for y in range(0, window_shape[1]):
            if np.dot(np.array([x, y]) - line_start, line_vector) < 0:
                mask[x, y] = 1

    # plt.imshow(mask.T, origin='lower')
    plt.imshow(mask)
    masked = np.ma.masked_where(mask == 1, mri_array[window[0]:window[1], window[2]:window[3]])

    fig3 = plt.figure()
    plt.imshow(mic_array[window[0]:window[1], window[2]:window[3], :], interpolation='nearest')
    plt.imshow(masked, cmap='gray', interpolation='nearest')
    plt.title('Composite')


    print('stuff')


if __name__ == '__main__':

    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch Super Res Example')
    parser.add_argument('-d', '--directory', type=str, required=True, help="Base directory for the rabbit")
    parser.add_argument('-b', '--block', type=str, required=True, help="Block to extract slices from")
    parser.add_argument('-s', '--slice', type=str, required=True, help="Slice to use from the block")
    parser.add_argument('-i', '--invivo', type=str, required=True, help="Path to the invivo directory")

    args = parser.parse_args()

    main(args)
