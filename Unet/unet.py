import torch
import torch.nn as nn
import torch.nn.init as init


class UNetDNBlock(nn.Module):
    def __init__(self, in_size, out_size, kernel_size=[3, 3]):
        super(UNetDNBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_size, out_size, kernel_size, padding=(1, 1))
        self.conv2 = nn.Conv2d(out_size, out_size, kernel_size, padding=(1, 1))
        # self.pool = nn.MaxPool2d(2)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.relu(self.conv1(x))
        x = self.relu(self.conv2(x))

        return x

    def _initialize_weights(self):
        init.xavier_normal_(self.conv1.weight, init.calculate_gain('relu'))
        init.xavier_normal_(self.conv2.weight, init.calculate_gain('relu'))


class UNetUPBlock(nn.Module):
    def __init__(self, in_size, out_size, kernel_size=[3, 3], pad=0):
        super(UNetUPBlock, self).__init__()
        self.convT = nn.ConvTranspose2d(in_size, out_size, 2, stride=2, output_padding=pad)
        self.conv1 = nn.Conv2d(in_size, out_size, kernel_size, padding=(1, 1))
        self.conv2 = nn.Conv2d(out_size, out_size, kernel_size, padding=(1, 1))
        self.relu = nn.ReLU()

    def forward(self, x, bridge):

        x = self.relu(self.convT(x))
        x = torch.cat([x, bridge], dim=1)
        x = self.relu(self.conv1(x))
        x = self.relu(self.conv2(x))

        return x

    def _initialize_weights(self):
        init.xavier_normal_(self.convT.weight, init.calculate_gain('relu'))
        init.xavier_normal_(self.conv1.weight, init.calculate_gain('relu'))
        init.xavier_normal_(self.conv2.weight, init.calculate_gain('relu'))


class UNet(nn.Module):
    def __init__(self):
        super(UNet, self).__init__()
        self.relu = nn.ReLU()
        self.pool = nn.MaxPool2d(2)

        self.conv1_dn = UNetDNBlock(9, 32)
        self.conv2_dn = UNetDNBlock(32, 64)
        self.conv3_dn = UNetDNBlock(64, 128)
        self.conv4_dn = UNetDNBlock(128, 256)

        self.middle = nn.Sequential(
            nn.Conv2d(256, 512, (3, 3), padding=(1, 1)),
            nn.Conv2d(512, 512, (3, 3), padding=(1, 1))
        )

        self.conv4_up = UNetUPBlock(512, 256)
        self.conv3_up = UNetUPBlock(256, 128)
        self.conv2_up = UNetUPBlock(128, 64)
        self.conv1_up = UNetUPBlock(64, 32)

        self.out_conv = nn.Conv2d(32, 1, (1, 1), padding=(0, 0))

        self._initialize_weights()

    def forward(self, x):

        bridge1 = self.conv1_dn(x)
        x = self.pool(bridge1)

        bridge2 = self.conv2_dn(x)
        x = self.pool(bridge2)

        bridge3 = self.conv3_dn(x)
        x = self.pool(bridge3)

        bridge4 = self.conv4_dn(x)
        x = self.pool(bridge4)

        x = self.middle(x)

        x = self.conv4_up(x, bridge4)
        x = self.conv3_up(x, bridge3)
        x = self.conv2_up(x, bridge2)
        x = self.conv1_up(x, bridge1)
        x = self.out_conv(x)

        return x

    def _initialize_weights(self):
        init.xavier_normal_(self.middle[0].weight, init.calculate_gain('relu'))
        init.xavier_normal_(self.middle[1].weight, init.calculate_gain('relu'))

