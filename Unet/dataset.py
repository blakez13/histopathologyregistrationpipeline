import torch
import torch.utils.data as data
import torchvision.transforms.functional as TF

from PIL import Image


def color_jitter(im):
    rand_hue = (torch.rand(1) - 0.5) * 0.3
    rand_sat = (torch.rand(1) * 0.2) + 0.9
    rand_gam = (torch.rand(1) * 0.2) + 0.9
    rand_con = (torch.rand(1) * 0.2) + 0.9
    rand_bri = (torch.rand(1) * 0.2) + 0.9
    rand_gray = torch.rand(1)

    im = TF.adjust_hue(im, rand_hue)
    im = TF.adjust_saturation(im, rand_sat)
    im = TF.adjust_gamma(im, rand_gam, 1.0)
    im = TF.adjust_contrast(im, rand_con)
    im = TF.adjust_brightness(im, rand_bri)
    if rand_gray >= 0.9:
        im = TF.to_grayscale(im, num_output_channels=3)

    return im


def transform(surface, scatter, difference, label):

    rand_h_flip = torch.rand(1)
    rand_v_flip = torch.rand(1)

    rand_rot = (torch.rand(1) * 30) - 15

    surface = color_jitter(surface)
    scatter = color_jitter(scatter)
    difference = color_jitter(difference)

    if rand_h_flip >= 0.5:
        surface = TF.hflip(surface)
        scatter = TF.hflip(scatter)
        difference = TF.hflip(difference)
        label = TF.hflip(label)
    if rand_v_flip >= 0.5:
        surface = TF.vflip(surface)
        scatter = TF.vflip(scatter)
        difference = TF.vflip(difference)
        label = TF.vflip(label)

    surface = TF.to_tensor(TF.rotate(surface, rand_rot))
    scatter = TF.to_tensor(TF.rotate(scatter, rand_rot))
    difference = TF.to_tensor(TF.rotate(difference, rand_rot))
    label = TF.to_tensor(TF.rotate(label, rand_rot))

    return surface, scatter, difference, label


class UnetDataset(data.Dataset):

    def __init__(self, surface, scatter, difference, label, testing=False):
        super(UnetDataset, self).__init__()

        self.scatter = scatter
        self.surface = surface
        self.difference = difference
        self.label = label
        self.testing = testing

    def __getitem__(self, item):

        # Load the given images
        surface = Image.open(self.surface[item])
        scatter = Image.open(self.scatter[item])
        difference = Image.open(self.difference[item])
        label = Image.open(self.label[item])

        # Transform the images if
        if self.testing:
            surface = TF.to_tensor(surface)
            scatter = TF.to_tensor(scatter)
            difference = TF.to_tensor(difference)
            label = TF.to_tensor(label)

        else:
            surface, scatter, difference, label = transform(surface, scatter, difference, label)

        # Concatenate the surface, scatter and difference
        source = torch.cat([surface, scatter, difference], 0)

        return source.float(), label.float().squeeze()

    def __len__(self):
        return len(self.scatter)
