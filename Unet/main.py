import argparse
from math import log10
import os
import sys
import glob
import time
import torch
import random
import torch.cuda
import numpy as np
import torch.nn as nn
import subprocess as sp
import torch.optim as optim
from Unet.unet import UNet
from Unet.dataset import UnetDataset
from Unet.externals import *
from torch.utils.data.sampler import RandomSampler, SequentialSampler
from torch.autograd import Variable
from torch.utils.data import DataLoader
from tensorboardX import SummaryWriter


# Training settings
parser = argparse.ArgumentParser(description='PyTorch Super Res Example')
parser.add_argument('--image_directory', type=str, required=True, help="Top directory for the images")
parser.add_argument('--label_directory', type=str, required=True, help="Top directory for the labels")
parser.add_argument('--train_batch', type=int, default=8, help='training batch size')
parser.add_argument('--test_batch', type=int, default=16, help='testing batch size')
parser.add_argument('--nEpochs', type=int, default=500, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.00001, help='Learning Rate. Default=0.01')
parser.add_argument('--split', type=float, default=0.8, help='Amount of data to use for training (0,1]')
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--augmentation_factor', type=int, default=10, help='Number of times to sample original list')
parser.add_argument('--threads', type=int, default=30, help='number of threads for data loader to use')
parser.add_argument('--scheduler', action='store_true', help='use scheduler?')
parser.add_argument('--seed', type=int, default=1234, help='random seed to use. Default=123')

opt = parser.parse_args()
print(opt)


def main(opt):
    def _get_branch(opt):
        p = sp.Popen(['git', 'rev-parse', '--abbrev-ref', 'HEAD'], shell=False, stdout=sp.PIPE)
        branch, _ = p.communicate()
        branch = branch.decode('utf-8').split()

        p = sp.Popen(['git', 'rev-parse', '--short', 'HEAD'], shell=False, stdout=sp.PIPE)
        hash, _ = p.communicate()
        hash = hash.decode('utf-8').split()

        opt.git_branch = branch
        opt.git_hash = hash

    def _check_branch(opt, params):
        """ When performing eval, check th git branch and commit that was used to generate the .pt file"""

        # Check the current branch and hash
        _get_branch(opt)

        if params.git_branch != opt.git_branch or params.git_hash != opt.git_hash:
            msg = 'You are not on the right branch or commit. Please run the following in the repository: \n'
            msg += f'git checkout {params.git_branch}\n'
            msg += f'git revert {params.git_hash}'
            sys.exit(msg)

    def generate_data_lists(opt):

        # Get the list of images for each image type
        surface_list = sorted(glob.glob(f'{opt.image_directory}/surface/*'))
        scatter_list = sorted(glob.glob(f'{opt.image_directory}/scatter/*'))
        difference_list = sorted(glob.glob(f'{opt.image_directory}/difference/*'))
        label_list = sorted(glob.glob(f'{opt.label_directory}/*'))

        # Zip the lists together so we can shuffle them all together at the same time
        zipped = list(zip(surface_list, scatter_list, difference_list, label_list))
        random.shuffle(zipped)
        surface_list, scatter_list, difference_list, label_list = list(zip(*zipped))

        return surface_list, scatter_list, difference_list, label_list

    def get_data_loaders(opt):

        surface_list, scatter_list, difference_list, label_list = generate_data_lists(opt)

        num_img = len(surface_list)
        test_split = int((num_img * opt.split))

        train_sampler = RandomSampler(range(0, test_split * opt.augmentation_factor))
        train_dataset = UnetDataset(surface_list[:test_split] * opt.augmentation_factor,
                                    scatter_list[:test_split] * opt.augmentation_factor,
                                    difference_list[:test_split] * opt.augmentation_factor,
                                    label_list[:test_split] * opt.augmentation_factor,
                                    testing=False)
        train_data_loader = DataLoader(train_dataset, opt.train_batch, sampler=train_sampler, num_workers=opt.threads)

        test_sampler = SequentialSampler(range(0, num_img - test_split))
        test_dataset = UnetDataset(surface_list[test_split:],
                                   scatter_list[test_split:],
                                   difference_list[:test_split],
                                   label_list[test_split:],
                                   testing=True)
        test_data_loader = DataLoader(test_dataset, opt.test_batch, sampler=test_sampler, num_workers=opt.threads)

        return train_data_loader, test_data_loader

    cuda = opt.cuda
    if cuda and not torch.cuda.is_available():
        raise Exception("No GPU found, please run without --cuda")

    timestr = time.strftime("%Y-%m-%d-%H%M%S")
    writer = SummaryWriter(f'{opt.image_directory}/../../runs/{timestr}/')
    writer.add_text('Parameters', opt.__str__())

    torch.manual_seed(opt.seed)
    if cuda:
        torch.cuda.manual_seed(opt.seed)

    print('===> Generating Data Loaders ... ', end='')
    train_data_loader, test_data_loader = get_data_loaders(opt)
    print('Done')

    print('===> Initiaing Model ... ', end='')
    model = UNet()
    crit = nn.BCEWithLogitsLoss()
    if cuda:
        model = torch.nn.DataParallel(model)
        model = model.cuda()
        crit = crit.cuda()

    optimizer = optim.SGD(model.parameters(), lr=opt.lr, weight_decay=1e-6, momentum=0.9, nesterov=True)

    if opt.scheduler:
        scheduler = CyclicLR(optimizer, step_size=700, max_lr=20*opt.lr, base_lr=opt.lr)

    print('Done')

    def train(epoch):
        model.train()
        epoch_loss = 0

        for iteration, batch in enumerate(train_data_loader, 1):
            scheduler.batch_step()
            input, target = Variable(batch[0]), Variable(batch[1])

            if cuda:
                input = input.cuda()
                target = target.cuda()

            optimizer.zero_grad()
            pred = model(input).squeeze()

            loss = crit(pred, target)
            epoch_loss += loss.item()
            loss.backward()
            optimizer.step()

            writer.add_scalar('Batch/Learning Rate', scheduler.get_lr()[-1],
                              (iteration + (len(train_data_loader) * (epoch - 1))))
            writer.add_scalar('Batch/Binary Cross Entropy Loss', loss.item(),
                              (iteration + (len(train_data_loader) * (epoch - 1))))
            print(
                "=> Done with {} / {}  Batch Loss: {:.6f}".format(iteration, len(train_data_loader), loss.item()))
        writer.add_scalar('Epoch/Cross Entropy Loss', epoch_loss / len(train_data_loader), epoch)
        print("===> Epoch {} Complete: Avg. Loss: {:.6f}".format(epoch, epoch_loss / len(train_data_loader)))

    def test(epoch):
        sigmoid = nn.Sigmoid()
        val_loss = 0
        dice = 0
        model.eval()
        print('===> Evaluating Model')
        with torch.no_grad():
            for iteration, batch in enumerate(test_data_loader, 1):
                input, target = Variable(batch[0]), Variable(batch[1])

                if cuda:
                    input = input.cuda()
                    target = target.cuda()

                pred = model(input).squeeze()

                loss = crit(pred, target).item()
                val_loss += loss
                pred = sigmoid(pred)

                mask = (pred > 0.5).float()

                dice += dice_loss(mask, target).item()

                if iteration == 1:
                    non_zero = [i for i, x in enumerate(target, 0) if x.sum() != 0]
                    if not non_zero:
                        writer.add_image('Test/Input', input[0, 0:3, :, :], epoch)
                        writer.add_image('Test/Probability', pred[0, :, :].repeat(3, 1, 1), epoch)
                        writer.add_image('Test/Prediction', mask[0, :, :].repeat(3, 1, 1), epoch)
                        writer.add_image('Test/Ground Truth', target[0, :, :].repeat(3, 1, 1), epoch)
                    else:
                        index = non_zero[0]
                        writer.add_image('Test/Input', input[index, 0:3, :, :], epoch)
                        writer.add_image('Test/Probability', pred[index, :, :].repeat(3, 1, 1), epoch)
                        writer.add_image('Test/Prediction', mask[index, :, :].repeat(3, 1, 1), epoch)
                        writer.add_image('Test/Ground Truth', target[index, :, :].repeat(3, 1, 1), epoch)
                print("=> Done with {} / {}  Batch Loss: {:.6f}".format(iteration, len(test_data_loader), loss))

        writer.add_scalar('test/Average Loss', val_loss / len(test_data_loader), epoch)
        writer.add_scalar('test/Average DICE', dice / len(test_data_loader), epoch)

        print("===> Avg. DICE: {:.6f}".format(dice / len(test_data_loader)))

    def checkpoint(state):
        path = f'{opt.image_directory}/../../output/{timestr}/epoch_{epoch}_model.pth'
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        torch.save(state, path)
        print("===> Checkpoint saved for Epoch {}".format(epoch))

    for epoch in range(1, opt.nEpochs):
        print("===> Learning Rate = {}".format(optimizer.param_groups[0]['lr']))
        train(epoch)
        if epoch % 10 == 0:
            checkpoint({
                'epoch': epoch,
                'scheduler': opt.scheduler,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict()})
        test(epoch)


if __name__ == '__main__':
    main(opt)
