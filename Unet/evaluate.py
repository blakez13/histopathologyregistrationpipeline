import os
import glob
import torch
import argparse
import numpy as np
import SimpleITK as sitk
import torchvision.transforms.functional as TF

from PIL import Image
from Unet.unet import UNet

# Training settings
parser = argparse.ArgumentParser(description='PyTorch Super Res Example')
parser.add_argument('--model_file', type=str, required=True, help="model file to load")
parser.add_argument('--image_directory', type=str, required=True, help="directory with the images to be predicted")
parser.add_argument('--output_directory', type=str, required=True, help="directory for saving")
# parser.add_argument('--train_batch', type=int, default=4, help='training batch size')
# parser.add_argument('--test_batch', type=int, default=2, help='testing batch size')
# parser.add_argument('--nEpochs', type=int, default=500, help='number of epochs to train for')
# parser.add_argument('--lr', type=float, default=0.00001, help='Learning Rate. Default=0.01')
# parser.add_argument('--split', type=float, default=0.8, help='Amount of data to use for training (0,1]')
# parser.add_argument('--cuda', action='store_true', help='use cuda?')
# parser.add_argument('--augmentation_factor', type=int, default=10, help='Number of times to sample original list')
# parser.add_argument('--threads', type=int, default=16, help='number of threads for data loader to use')
# parser.add_argument('--scheduler', action='store_true', help='use scheduler?')
# parser.add_argument('--seed', type=int, default=1234, help='random seed to use. Default=123')

opt = parser.parse_args()
print(opt)


def preprocess_mhd_image(image_path):
    itk_image = sitk.ReadImage(image_path)
    numpy_image = np.squeeze(sitk.GetArrayFromImage(itk_image))
    numpy_image /= np.max(numpy_image)
    numpy_image = (numpy_image * 255).astype('uint8')
    return Image.fromarray(numpy_image, mode='RGB')


def postprocess_pil_image(pil_Image, path, opt):

    # We need to resize the image to the original size
    pil_Image = pil_Image.resize(original_size, Image.ANTIALIAS)

    # Define the output name
    image_prefix = '_'.join(path.split('/')[-1].split('_')[0:2])
    prefix = f'{path.split("/")[-3]}/{image_prefix}_prediction.mhd'

    if not os.path.exists(f'{opt.output_directory}{path.split("/")[-3]}'):
        os.makedirs(f'{opt.output_directory}{path.split("/")[-3]}')

    # Convert the image to itk so we can use mhd format
    np_Image = np.array(pil_Image)
    # np_Image /= np.max(np_Image)
    itk_Image = sitk.GetImageFromArray(np_Image)
    sitk.WriteImage(itk_Image, f'{opt.output_directory}/{prefix}')


def images_to_tensor(surface, scatter):
    surf_Image = preprocess_mhd_image(surface)
    scat_Image = preprocess_mhd_image(scatter)
    global original_size
    original_size = surf_Image.size

    # Downsample the image by factor of 2
    surf_Image = surf_Image.resize((1024, 1536), Image.ANTIALIAS)
    scat_Image = scat_Image.resize((1024, 1536), Image.ANTIALIAS)

    surf_Tensor = TF.to_tensor(surf_Image)
    scat_Tensor = TF.to_tensor(scat_Image)

    source = torch.cat([surf_Tensor, scat_Tensor], 0)

    return source.unsqueeze(0)


def eval(tensor, model):

    sigmoid = torch.nn.Sigmoid()

    with torch.no_grad():
        pred = model(tensor)
        pred = sigmoid(pred).squeeze(1)
        pred_Image = TF.to_pil_image(pred.cpu())

    return pred_Image


def main(opt):
    hd_root = '/hdscratch/ucair/blockface/'

    print('===> Initiating and Loading the Model ... ', end='')
    model = UNet()
    model.eval()
    model = torch.nn.DataParallel(model)
    model = model.cuda()
    params = torch.load(opt.model_file)
    model.load_state_dict(params['state_dict'])
    print('Done')

    # Get the filelist
    surface_list = sorted(glob.glob(f'{opt.image_directory}surface/*.mhd'))
    scatter_list = sorted(glob.glob(f'{opt.image_directory}scatter/*.mhd'))

    for surf, scat in zip(surface_list, scatter_list):
        source = images_to_tensor(surf, scat)
        pred_Image = eval(source, model)
        postprocess_pil_image(pred_Image, surf, opt)


if __name__ == '__main__':
    main(opt)
