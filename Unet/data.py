import os
import glob
import numpy as np
import SimpleITK as sitk


from PIL import Image


def preprocess_mhd_image(image_path):
    itk_image = sitk.ReadImage(image_path)
    numpy_image = np.squeeze(sitk.GetArrayFromImage(itk_image))
    numpy_image /= np.max(numpy_image)
    numpy_image = (numpy_image * 255).astype('uint8')
    return Image.fromarray(numpy_image, mode='RGB')


def preprocess_tif_image(image_path):
    itk_image = sitk.ReadImage(image_path)
    numpy_image = np.squeeze(sitk.GetArrayFromImage(itk_image))
    numpy_image = (numpy_image * 255).astype('uint8')
    return Image.fromarray(numpy_image)


def complile_training_data(rabbits):

    hd_root = '/hdscratch/ucair/blockface/'
    sd_root = '/scratch/ucair/blockface/trainingData/'

    # Create some empty lists to fill
    surface_pwds = []
    scatter_pwds = []
    difference_pwds = []
    label_pwds = []

    for rabbit in rabbits:

        # Get the list of blocks that have segmentations
        block_pwds = sorted(glob.glob(f'{hd_root}{rabbit}/labels/*'))
        blocks = [x.split('/')[-1] for x in block_pwds]

        for block in blocks:
            # Get the image lists for the block
            surface_pwds += sorted(glob.glob(f'{hd_root}{rabbit}/affineImages/{block}/surface/*.mhd'))
            scatter_pwds += sorted(glob.glob(f'{hd_root}{rabbit}/affineImages/{block}/scatter/*.mhd'))
            difference_pwds += sorted(glob.glob(f'{hd_root}{rabbit}/affineImages/{block}/difference/*.mhd'))

            # Get the list of labels
            label_pwds += sorted(glob.glob(f'{hd_root}{rabbit}/labels/{block}/*.tif'))

            # Make sure that there is a block directory to save to
            if not os.path.exists(f'{sd_root}images/'):
                os.makedirs(f'{sd_root}images/surface/')
                os.makedirs(f'{sd_root}images/scatter/')
                os.makedirs(f'{sd_root}images/difference/')

            if not os.path.exists(f'{sd_root}labels/'):
                os.makedirs(f'{sd_root}labels/')

            for surf, scat, diff, label in zip(surface_pwds, scatter_pwds, difference_pwds, label_pwds):
                # Define the path to the output folder
                surf_Image = preprocess_mhd_image(surf)
                scat_Image = preprocess_mhd_image(scat)
                diff_Image = preprocess_mhd_image(diff)
                label_Image = preprocess_tif_image(label)

                # Get the image prefix
                surf_prefix = surf.split('/')[-1].split('.')[0]
                scat_prefix = scat.split('/')[-1].split('.')[0]
                diff_prefix = diff.split('/')[-1].split('.')[0]
                label_prefix = label.split('/')[-1].split('.')[0]

                size = surf_Image.size

                # Downsample the image by factor of 2
                surf_Image = surf_Image.resize((1024, 1536), Image.ANTIALIAS)
                scat_Image = scat_Image.resize((1024, 1536), Image.ANTIALIAS)
                diff_Image = diff_Image.resize((1024, 1536), Image.ANTIALIAS)
                label_Image = label_Image.resize((1024, 1536), Image.ANTIALIAS)

                # Save the images
                surf_Image.save(f'{sd_root}/images/surface/{surf_prefix}_{rabbit}_{block}.tif',
                                compression=None)
                scat_Image.save(f'{sd_root}/images/scatter/{scat_prefix}_{rabbit}_{block}.tif',
                                compression=None)
                diff_Image.save(f'{sd_root}/images/difference/{diff_prefix}_{rabbit}_{block}.tif',
                                compression=None)
                label_Image.save(f'{sd_root}/labels/{label_prefix}_{rabbit}_{block}.tif',
                                 compression=None)


if __name__ == '__main__':
    complile_training_data(['18_047'])
