import os
import sys
import glob
import time
import torch
import numpy as np
import torch.nn as nn
import subprocess as sp
import torch.optim as optim
import SimpleITK as sitk
import torch.nn.functional as F

import matplotlib

from .vNetModel import *

matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

from types import SimpleNamespace
from math import floor
from .losses import dice_loss
from .dataset import TrainDataset, EvalDataset
from .learningrate import CyclicLR
from torch.autograd import Variable
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler, SequentialSampler


def _check_dims(tensor_list):
    if tensor_list[0].dim() == 4:
        x = [x.shape[1] >= 64 for x in tensor_list]
    elif tensor_list[0].dim() == 3:
        x = [x.shape[0] >= 64 for x in tensor_list]

    if x:
        return
    else:
        raise ValueError('A Z dimension is not greater than or equal to 64.')


def _get_branch(opt):
    p = sp.Popen(['git', 'rev-parse', '--abbrev-ref', 'HEAD'], shell=False, stdout=sp.PIPE)
    branch, _ = p.communicate()
    branch = branch.decode('utf-8').split()

    p = sp.Popen(['git', 'rev-parse', '--short', 'HEAD'], shell=False, stdout=sp.PIPE)
    hash, _ = p.communicate()
    hash = hash.decode('utf-8').split()

    opt.git_branch = branch
    opt.git_hash = hash


def _check_branch(opt, params):
    """ When performing eval, check th git branch and commit that was used to generate the .pt file"""

    # Check the current branch and hash
    _get_branch(opt)

    if params.git_branch != opt.git_branch or params.git_hash != opt.git_hash:
        msg = 'You are not on the right branch or commit. Please run the following in the repository: \n'
        msg += f'git checkout {params.git_branch}\n'
        msg += f'git revert {params.git_hash}'
        sys.exit(msg)


# We need to preprocess the data so all of the blocks have at least 64 in the Z dimension
def _pre_process(source_tensor_list, target_tensor_list, opt):
    # We don't want to track any of this
    with torch.no_grad():
        # create a convolution layer
        conv = nn.Conv3d(1, 1, (3, 1, 1), (2, 1, 1), bias=False)
        conv.weight[:, :, 0, :, :] = 0.5
        conv.weight[:, :, 1, :, :] = 0.0
        conv.weight[:, :, 2, :, :] = 0.5

        for item in range(0, len(source_tensor_list)):
            source_tensor = source_tensor_list[item]
            target_tensor = target_tensor_list[item]
            shape = source_tensor.shape

            if shape[1] >= opt.cube[0]:
                pass
            else:
                up_source = torch.zeros(shape[0], (shape[1] * 2 - 1), shape[2], shape[3])
                up_target = torch.zeros((shape[1] * 2 - 1), shape[2], shape[3])
                # Set every other slice to the originial
                up_source[:, ::2, :, :] = source_tensor
                up_target[::2, :, :] = target_tensor
                # For the target we can just replicate the slices as it is all 0&1
                up_target[1::2, :, :] = target_tensor[:-1]

                # Have to do it channel by channel or it sums ths channels together?
                for ch in range(0, 3):
                    channel = up_source[ch, :, :, :]

                    # ch = ch.detach()
                    channel = conv(channel.unsqueeze(0).unsqueeze(0)).squeeze(0)
                    up_source[ch, 1::2, :, :] = channel

                source_tensor_list[item] = up_source
                target_tensor_list[item] = up_target

        _check_dims(source_tensor_list)
        _check_dims(target_tensor_list)


def get_loader(source, target, state, opt, stride=None):
    data_split = int(floor(len(source) * 0.8))
    rep_factor = 200

    if state == 'train':
        source_list = source[0:data_split]
        target_list = target[0:data_split]
        dataset = TrainDataset(source_list, target_list, opt.cube, rep_factor)
        sampler = SubsetRandomSampler(range(0, len(dataset)))
        return DataLoader(dataset, opt.trainBatchSize, sampler=sampler, num_workers=opt.threads)

    elif state == 'infer':
        source_list = source[-(len(source) - data_split):]
        target_list = target[-(len(source) - data_split):]
        dataset = TrainDataset(source_list, target_list, opt.cube, rep_factor)
        sampler = SequentialSampler(range((data_split * rep_factor), (data_split * rep_factor) + len(dataset)))
        return DataLoader(dataset, opt.inferBatchSize, sampler=sampler, num_workers=opt.threads)

    elif state == 'eval':
        dataset = EvalDataset(source, opt.cube, stride=stride)
        sampler = SequentialSampler(range(0, len(dataset)))
        return DataLoader(dataset, opt.evalBatchSize, sampler=sampler, num_workers=opt.threads)


def train(opt):

    # Add the git information to the opt
    _get_branch(opt)
    timestr = time.strftime("%Y-%m-%d-%H%M%S")
    writer = SummaryWriter('./VNet/runs/{0}/'.format(timestr))
    writer.add_text('Parameters', opt.__str__())
    dataDir = '/home/sci/blakez/ucair/histopathology/vnetData/'

    try:
        os.stat('./VNet/output/{0}/'.format(timestr))
    except OSError:
        os.makedirs('./VNet/output/{0}/'.format(timestr))

    # Load the data
    print('===> Loading Data')
    sourceTensor = []
    labelTensor = []

    for file in sorted(glob.glob(dataDir + 'blocks/*.pt')):
        sourceTensor.append(torch.load(file))
    for file in sorted(glob.glob(dataDir + 'masks/*.pt')):
        labelTensor.append(torch.load(file))

    # Just make sure that the block from 18_044 is in the training data
    sourceTensor[-1], sourceTensor[0] = sourceTensor[0], sourceTensor[-1]
    labelTensor[-1], labelTensor[0] = labelTensor[0], labelTensor[-1]

    print('===> Preprocessing Data')
    _pre_process(sourceTensor, labelTensor, opt)

    cuda = opt.cuda
    if cuda and not torch.cuda.is_available():
        raise Exception("No GPU found, please run without --cuda")

    print('===> Generating Datasets')
    training_data_loader = get_loader(sourceTensor, labelTensor, 'train', opt)
    infering_data_loader = get_loader(sourceTensor, labelTensor, 'infer', opt)

    # I think we can do just 1 class
    # classes = 1

    model = vnet_model.VNet(3, 2)
    # sig = nn.Sigmoid()
    # threshold = nn.Threshold(0.5, 0)
    # crit = nn.NLLLoss().cuda()
    crit = nn.CrossEntropyLoss().cuda()
    # crit = nn.BCEWithLogitsLoss()
    # optimizer = optim.Adam(model.parameters(), lr=opt.lr)
    optimizer = optim.SGD(model.parameters(), lr=opt.lr, weight_decay=1e-6, momentum=0.9, nesterov=True)
    if cuda:
        model = torch.nn.DataParallel(model)
        model = model.cuda()

    if opt.scheduler:
        scheduler = CyclicLR(optimizer, step_size=200, max_lr=10*opt.lr, base_lr=opt.lr)
        # scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', patience=10)

    if opt.resume:
        params = torch.load(opt.ckpt)
        model.load_state_dict(params['state_dict'])
        optimizer.load_state_dict(params['optimizer'])
        # if params['scheduler']:
        #     scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', patience=10)

    def checkpoint(state):
        path = './VNet/output/{0}/epoch_{1}_model.pth'.format(timestr, epoch)
        torch.save(state, path)
        print("===> Checkpoint saved for Epoch {}".format(epoch))

    def learn(epoch):
        model.train()
        epoch_loss = 0

        for iteration, batch in enumerate(training_data_loader, 1):
            scheduler.batch_step()
            input, target = Variable(batch[0]), Variable(batch[1])

            if cuda:
                input = input.cuda()
                target = target.cuda()

            optimizer.zero_grad()
            pred = model(input)

            loss = crit(pred, target.long())
            epoch_loss += loss.item()
            loss.backward()
            optimizer.step()

            writer.add_scalar('Batch/Learning Rate', scheduler.get_lr()[-1],
                              (iteration + (len(training_data_loader) * (epoch - 1))))
            writer.add_scalar('Batch/Cross Entropy Loss', loss.item(),
                              (iteration + (len(training_data_loader) * (epoch - 1))))
            print(
                "=> Done with {} / {}  Batch Loss: {:.6f}".format(iteration, len(training_data_loader), loss.item()))
        writer.add_scalar('Epoch/Cross Entropy Loss', epoch_loss / len(training_data_loader), epoch)
        print("===> Epoch {} Complete: Avg. Loss: {:.6f}".format(epoch, epoch_loss / len(training_data_loader)))

    def infer(epoch):
        softmax = nn.Softmax(dim=1)
        val_loss = 0
        dice = 0
        model.eval()
        print('===> Evaluating Model')
        with torch.no_grad():
            for iteration, batch in enumerate(infering_data_loader, 1):
                input, target = Variable(batch[0]), Variable(batch[1])

                if cuda:
                    input = input.cuda()
                    target = target.cuda()

                pred = model(input)

                loss = crit(pred, target.long()).item()
                val_loss += loss

                # Turn the prediction into probabilities
                pred = softmax(pred)
                mask = (pred[:, 1, :, :, :] > pred[:, 0, :, :, :]).float()

                dice += dice_loss(mask, target.float()).item()

                if iteration == 1:
                    non_zero = [i for i, x in enumerate(target, 0) if x.sum() != 0]
                    if not non_zero:
                        writer.add_image('Test/Input', input[1, :, 32, :, :].squeeze(), epoch)
                        # writer.add_image('Test/Probability FG', fg[1, 32, :, :].repeat(3, 1, 1), epoch)
                        writer.add_image('Test/Prediction', mask[1, 32, :, :].repeat(3, 1, 1), epoch)
                        # writer.add_image('Test/Mask', (fg[1, 32, :, :] > 0.5).float().repeat(3, 1, 1), epoch)
                        writer.add_image('Test/Ground Truth', target[1, 32, :, :].repeat(3, 1, 1), epoch)
                    else:
                        index = non_zero[0]
                        writer.add_image('Test/Input', input[index, :, 32, :, :].squeeze(), epoch)
                        # writer.add_image('Test/Probability FG', fg[1, 32, :, :].repeat(3, 1, 1), epoch)
                        writer.add_image('Test/Prediction', mask[index, 32, :, :].repeat(3, 1, 1), epoch)
                        # writer.add_image('Test/Mask', (fg[1, 32, :, :] > 0.5).float().repeat(3, 1, 1), epoch)
                        writer.add_image('Test/Ground Truth', target[index, 32, :, :].repeat(3, 1, 1), epoch)
                print("=> Done with {} / {}  Batch Loss: {:.6f}".format(iteration, len(infering_data_loader), loss))

        writer.add_scalar('test/Average Loss', val_loss / len(infering_data_loader), epoch)
        writer.add_scalar('test/Average DICE', dice / len(infering_data_loader), epoch)

        print("===> Avg. DICE: {:.6f}".format(dice / len(infering_data_loader)))

    print("===> Beginning Training")

    if opt.resume:
        epochs = range(params['epoch'], opt.nEpochs)
    else:
        epochs = range(1, opt.nEpochs)

    for epoch in epochs:
        print("===> Learning Rate = {}".format(optimizer.param_groups[0]['lr']))
        learn(epoch)
        if epoch % 5 == 0:
            checkpoint({
                'epoch': epoch,
                'scheduler': opt.scheduler,
                'git_branch': opt.git_branch,
                'git_hash': opt.git_hash,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict()})
        infer(epoch)
        # if opt.scheduler:
        #     scheduler.step(epoch)


def eval(opt):
    # Load the data
    image_list = sorted(glob.glob(opt.image_dir + '/*.tif'))
    reader = sitk.ImageFileReader()

    images = []

    for img in image_list:
        reader.SetFileName(img)
        imgArray = sitk.GetArrayFromImage(reader.Execute())
        images.append(imgArray[:, :, 0:3])

    volume = np.stack(images)
    volume = volume / volume.max()
    volume = volume.transpose(3, 0, 1, 2)

    source_tensor = torch.from_numpy(volume)
    source_tensor = source_tensor.float()
    target_tensor = torch.zeros(source_tensor.shape)
    target_tensor = target_tensor[0, :, :, :].squeeze(0)

    source_list = [source_tensor]
    target_list = [target_tensor]
    _pre_process(source_list, target_list, opt)

    print('===> Loading Datasets')
    # Get a loader
    eval_loader = get_loader(source_list, target_list, state='eval', opt=opt, stride=[64, 128, 128])

    model = vnet_model.VNet(3, 2)
    # sig = nn.Sigmoid()
    # crit = nn.NLLLoss().cuda()
    # crit = nn.CrossEntropyLoss().cuda()
    #
    # if opt.cuda:
    #     model = torch.nn.DataParallel(model)
    #     model = model.cuda()

    if opt.cuda:
        model = torch.nn.DataParallel(model)
        model = model.cuda()
        params = torch.load(opt.ckpt)
        model.load_state_dict(params['state_dict'])
    else:
        params = torch.load(opt.ckpt, map_location=lambda storage, loc: storage)
        model.load_state_dict(params['state_dict'])

    model.eval()

    preds = []
    softmax = nn.Softmax(dim=1)
    print('===> Evaluating')
    with torch.no_grad():
        for iteration, batch in enumerate(eval_loader, 1):
            input = batch

            if opt.cuda:
                input = input.cuda()

            # torch.backends.cudnn.enabled = False
            pred = model(input)

            pred = softmax(pred)
            pred = (pred[:, 1, :, :, :] > pred[:, 0, :, :, :]).float()

            preds.append(pred.cpu())

    print('===> Filling')
    info = SimpleNamespace(**eval_loader.dataset.info)
    prediction = torch.cat(preds, 0).cuda('cuda:1')
    # prediction = prediction[:, 1, :, :, :].squeeze()

    # Empty the cache for more
    torch.cuda.empty_cache()

    # Need to fix the 2 dimension in the front - isn't guaranteed
    # Explicitly separate the front and back first
    prediction = prediction.view(2, prediction.shape[0] // 2, opt.cube[0], opt.cube[1], opt.cube[2])

    # Break it into the separate blocks that were evaluated
    prediction = prediction.contiguous()
    prediction = prediction.view(2, info.num_blocks[0], info.num_blocks[1], opt.cube[0], opt.cube[1], opt.cube[2])

    # Permute the blocks to the correct dimensions
    prediction = prediction.permute(0, 3, 1, 4, 2, 5)

    # Now we have to split it into the separate pieces to deal with any stride
    # Currently only handles stide different of 2x
    if info.stride != opt.cube:
        pred_stride1 = prediction[:, :, ::2, :, ::2, :]
        pred_stride2 = prediction[:, :, 1::2, :, 1::2, :]
        s1b = [pred_stride1.shape[2], pred_stride1.shape[4]]
        s2b = [pred_stride2.shape[2], pred_stride2.shape[4]]

        pred_stride1 = pred_stride1.contiguous().view(2, opt.cube[0], s1b[0] * opt.cube[1], s1b[1] * opt.cube[2])
        pred_stride2 = pred_stride2.contiguous().view(2, opt.cube[0], s2b[0] * opt.cube[1], s2b[1] * opt.cube[2])

        # Something is weird about this
        pad = nn.ConstantPad3d((0, 0,
                               (opt.cube[1] - info.stride[1]) // 2, (opt.cube[1] - info.stride[1]) // 2,
                               (opt.cube[2] - info.stride[2]) // 2, (opt.cube[2] - info.stride[2]) // 2), -1)

    else:
        prediction = prediction.contiguous().view(2, opt.cube[0],
                                                  info.num_blocks[0] * opt.cube[1],
                                                  info.num_blocks[1] * opt.cube[2])

    # Have to deal with this front back stuff
    vol = -1 * torch.ones(info.orig_shape[1], info.orig_shape[2] + info.x_pad,
                          info.orig_shape[3] + info.y_pad).cuda('cuda:1')

    vol[0:opt.cube[0]] = prediction.contiguous()[0]
    vol_block = vol[-opt.cube[0]:]
    pred_block = prediction[1]

    vol_block[vol_block != -1.0] = torch.max(pred_block[vol_block != -1.0], vol_block[vol_block != -1.0])
    vol_block[vol_block == -1.0] = pred_block[vol_block == -1.0]

    # Change back to cpu()

    vol = vol[:, info.x_pad // 2: -(info.x_pad - (info.x_pad // 2)), info.y_pad // 2: -(info.y_pad - (info.y_pad // 2))]
    # Downsample back to the original resolution? - NOT ALWAYS
    if info.orig_size[0] < 64:
        vol = vol[::2, :, :]
    vol = vol.permute(0, 2, 1).contiguous()
    vol = vol.cpu()
    itkImage = sitk.GetImageFromArray(vol.numpy(), isVector=False)
    itkImage.SetSpacing((opt.spacing[0], opt.spacing[1], opt.spacing[2])) #(opt.spacing[2] * opt.size[2]) / vol.shape[0]))
    itkImage.SetOrigin(opt.origin)
    sitk.WriteImage(itkImage, opt.out_name, True)


    print('===> Evaluation Complete')


if __name__ == '__main__':
    trainOpt = {'trainBatchSize': 2,
                'inferBatchSize': 2,
                'nEpochs': 100,
                'lr': 0.001,
                'cuda': True,
                'threads': 16,
                'cube': [64, 256, 256],
                'resume': False,
                'scheduler': True,
                'ckpt': '/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/VNet/output/2019-02-08-182207/epoch_210_model.pth',
                }

    evalOpt = {'evalBatchSize': 4,
               'image_dir': '/home/sci/blakez/ucair/histopathology/18_044/blockface/affineImages/',
               'cuda': True,
               'threads': 0,
               'cube': [64, 256, 256],
               'ckpt': '/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/VNet/output/2019-02-10-174756/epoch_20_model.pth',
               }

    evalOpt = SimpleNamespace(**evalOpt)
    trainOpt = SimpleNamespace(**trainOpt)

    # main(trainOpt)
    eval(evalOpt)
    print('All Done')
