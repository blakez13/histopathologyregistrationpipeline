import time
import json
import seg3d2

with open('/home/sci/blakez/ucair/histopathology/generate_isosurface_temp.json', 'r') as f:
    indict = json.load(f)

inputF = indict['inputF']
upperT = indict['upperT']
lowerT = indict['lowerT']
outputF = indict['outputF']

_ = seg3d2.importlayer(filename=inputF, importer='[Teem Importer]', mode='data', inputfiles_id='1')
_ = seg3d2.threshold(layerid='layer_0', lower_threshold=lowerT, upper_threshold=upperT)
time.sleep(2.0)
_ = seg3d2.computeisosurface(layerid='layer_1')
time.sleep(2.0)
_ = seg3d2.exportisosurface(layer='layer_1', file_path=outputF)
