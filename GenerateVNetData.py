import os
import glob
import torch
import numpy as np
import SimpleITK as sitk

import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
plt.ion()


def _check(list, path):
    """ Function for checking if new data has been generated for training """

    # Determine what blocks from what rabbit we already have
    data = sorted(glob.glob(path + f'masks/*.pt'))
    names = [x.split('/')[-1] for x in data]
    pairs = [x.split('_')[0] + '_' + x.split('_')[1] + '-' + x.split('_')[2] for x in names]

    not_done = [x for x in list if x.split('/')[-4] + '-' + x.split('/')[-1] not in pairs]

    return not_done


def _stack_images(path, block):
    # Initiate the reader
    reader = sitk.ImageFileReader()

    # Create an empty image list
    imageList = []

    # Loop over the affine directory
    for img in sorted(glob.glob(path + f'/{block}/*.tif')):
        reader.SetFileName(img)
        imgArray = sitk.GetArrayFromImage(reader.Execute())
        imageList.append(imgArray[:, :, 0:3])
    # Now need to stack the list into one array
    blockVolume = np.stack(imageList)

    # Normalize the blockVolume
    blockVolume = blockVolume / blockVolume.max()

    return blockVolume


def _read_mask(path):

    # Initiate the reader
    reader = sitk.ImageFileReader()
    block = path.split('/')[-2]

    reader.SetFileName(path + f'{block}_mask.nrrd')
    maskVolume = sitk.GetArrayFromImage(reader.Execute()).transpose(0, 2, 1)

    return maskVolume


def generate(blockList):

    # Dir = '/home/sci/blakez/ucair/histopathology/'
    # # Get a list of all the blocks for all the rabbits
    # rabbits = sorted(glob.glob(Dir + '/18*'))
    out = '/home/sci/blakez/ucair/histopathology/vnetData/'
    #
    # blockList = []
    #
    # # Get a list of all the block
    # for rabbit in rabbits:
    #     blockList += sorted(glob.glob(rabbit + '/blockface/volumes/*'))

    # Check the list with data that has already been generated
    blockList = _check(blockList, out)

    for block in blockList:
        rabbitName = block.split('/')[-4]
        blockName = block.split('/')[-1]
        rabbit = f'/home/sci/blakez/ucair/histopathology/{rabbitName}'

        if os.path.exists(rabbit + f'/blockface/volumes/{blockName}/{blockName}_mask.nrrd'):
            maskVolume = _read_mask(rabbit + f'/blockface/volumes/{blockName}/')
            maskTensor = torch.from_numpy(maskVolume)
            torch.save(maskTensor, out + f'masks/{rabbitName}_{blockName}_mask.pt')

            dataVolume = _stack_images(rabbit + f'/blockface/affineImages', blockName)
            dataVolume = dataVolume.transpose(3, 0, 1, 2)
            dataVolume = torch.from_numpy(dataVolume)
            torch.save(dataVolume, out + f'blocks/{rabbitName}_{blockName}_data.pt')
        else:
            pass

    print('All Available Data Generated ')


if __name__ == '__main__':
    generate()
