# trace generated using paraview version 5.6.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
from paraview import servermanager
import numpy as np
import glob
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Define the file name
blockface_file = '/hdscratch/ucair/blockface/18_047/affineImages/block10/surface/IMG_020_surface.mhd'
mr_file = '/home/sci/blakez/ucair/18_047/rawVolumes/PostImaging_2018-07-02/008_----_t2_spc_1mm_iso_cor.mhd'
# mr_file = '/home/sci/blakez/ucair/18_047/rawVolumes/ExVivo_2018-07-26/011_----_3D_VIBE_0p5iso_cor_3ave.mhd'
tform_dir = '/hdscratch/ucair/blockface/18_047/surfaces/rigid/'
mic_dir = '/hdscratch/ucair/microscopic/18_047/registered/'

blocks = [x.split('/')[-1] for x in sorted(glob.glob(mic_dir + '*'))]
# blocks = [x for x in blocks if x != 'block04']
blocks = blocks[6:]
# blocks = blocks[0:2]

# create a new 'Meta File Series Reader'
MR_Data = MetaFileSeriesReader(FileNames=[mr_file])

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [2695, 1278]

# show data in view
MR_Display = Show(MR_Data, renderView1)

# trace defaults for the display properties.
MR_Display.Representation = 'Outline'
MR_Display.ColorArrayName = ['POINTS', '']
MR_Display.OSPRayScaleArray = 'MetaImage'
MR_Display.OSPRayScaleFunction = 'PiecewiseFunction'
MR_Display.SelectOrientationVectors = 'MetaImage'
MR_Display.ScaleFactor = 12.775
MR_Display.SelectScaleArray = 'MetaImage'
MR_Display.GlyphType = 'Arrow'
MR_Display.GlyphTableIndexArray = 'MetaImage'
MR_Display.GaussianRadius = 0.63875
MR_Display.SetScaleArray = ['POINTS', 'MetaImage']
MR_Display.ScaleTransferFunction = 'PiecewiseFunction'
MR_Display.OpacityArray = ['POINTS', 'MetaImage']
MR_Display.OpacityTransferFunction = 'PiecewiseFunction'
MR_Display.DataAxesGrid = 'GridAxesRepresentation'
MR_Display.SelectionCellLabelFontFile = ''
MR_Display.SelectionPointLabelFontFile = ''
MR_Display.PolarAxes = 'PolarAxesRepresentation'
MR_Display.ScalarOpacityUnitDistance = 0.6642375793301755
MR_Display.IsosurfaceValues = [930.0]
MR_Display.Slice = 159

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
MR_Display.DataAxesGrid.XTitleFontFile = ''
MR_Display.DataAxesGrid.YTitleFontFile = ''
MR_Display.DataAxesGrid.ZTitleFontFile = ''
MR_Display.DataAxesGrid.XLabelFontFile = ''
MR_Display.DataAxesGrid.YLabelFontFile = ''
MR_Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
MR_Display.PolarAxes.PolarAxisTitleFontFile = ''
MR_Display.PolarAxes.PolarAxisLabelFontFile = ''
MR_Display.PolarAxes.LastRadialAxisTextFontFile = ''
MR_Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# get color transfer function/color map for 'MetaImage'
metaImageLUT = GetColorTransferFunction('MetaImage')

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
metaImageLUT.ApplyPreset('Grayscale', True)

# update the view to ensure updated data information
renderView1.Update()

# now we need to loop over the blocks
Block_list = []
TForm_list = []
Calc_list = []
Thresh_list = []

for i, block in enumerate(blocks):

    # create a new 'Meta File Series Reader'
    Block_list.append(MetaFileSeriesReader(FileNames=[mic_dir + block + '/' + block + '_histopathology_affine_volume.mhd']))

    # show data in view
    block_display = Show(Block_list[i], renderView1)

    # get color transfer function/color map for 'MetaImage'
    metaImageLUT = GetColorTransferFunction('MetaImage')

    # get opacity transfer function/opacity map for 'MetaImage'
    metaImagePWF = GetOpacityTransferFunction('MetaImage')

    # trace defaults for the display properties.
    block_display.Representation = 'Slice'
    block_display.ColorArrayName = ['POINTS', 'MetaImage']
    block_display.LookupTable = metaImageLUT
    block_display.OSPRayScaleArray = 'MetaImage'
    block_display.OSPRayScaleFunction = 'PiecewiseFunction'
    block_display.SelectOrientationVectors = 'MetaImage'
    block_display.ScaleFactor = 5.8667921859771015
    block_display.SelectScaleArray = 'MetaImage'
    block_display.GlyphType = 'Arrow'
    block_display.GlyphTableIndexArray = 'MetaImage'
    block_display.GaussianRadius = 0.29333960929885505
    block_display.SetScaleArray = ['POINTS', 'MetaImage']
    block_display.ScaleTransferFunction = 'PiecewiseFunction'
    block_display.OpacityArray = ['POINTS', 'MetaImage']
    block_display.OpacityTransferFunction = 'PiecewiseFunction'
    block_display.DataAxesGrid = 'GridAxesRepresentation'
    block_display.SelectionCellLabelFontFile = ''
    block_display.SelectionPointLabelFontFile = ''
    block_display.PolarAxes = 'PolarAxesRepresentation'
    block_display.ScalarOpacityUnitDistance = 0.3826002566541143
    block_display.ScalarOpacityFunction = metaImagePWF
    block_display.IsosurfaceValues = [0.5]

    # init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
    block_display.DataAxesGrid.XTitleFontFile = ''
    block_display.DataAxesGrid.YTitleFontFile = ''
    block_display.DataAxesGrid.ZTitleFontFile = ''
    block_display.DataAxesGrid.XLabelFontFile = ''
    block_display.DataAxesGrid.YLabelFontFile = ''
    block_display.DataAxesGrid.ZLabelFontFile = ''

    # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
    block_display.PolarAxes.PolarAxisTitleFontFile = ''
    block_display.PolarAxes.PolarAxisLabelFontFile = ''
    block_display.PolarAxes.LastRadialAxisTextFontFile = ''
    block_display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

    # show color bar/color legend
    block_display.SetScalarBarVisibility(renderView1, True)

    # update the view to ensure updated data information
    renderView1.Update()

    # Properties modified on block_display
    block_display.MapScalars = 0
    #
    # ## Stopping here to test
    # t1 = servermanager.CreateProxy("transforms", "Transform")
    # tp1 = servermanager._getPyProxy(t1)
    #
    # distance = int(blockface_file.split('/')[-1].split('_')[1]) * 0.05
    # z_translate = np.eye(4)
    # z_translate[2, 3] = distance
    # z_translate = [item for sublist in z_translate.tolist() for item in sublist]
    # tp1.Matrix = z_translate
    #
    # transform = Transform(Input=Block_list[i])
    # transform.Transform = tp1
    #
    # SetActiveSource(transform)

    # Now load the transfrom from the file
    # block = blockface_file.split('/')[-3]
    tform_file = tform_dir + block + '/' + block + '_to_invivo_paraview.txt'
    
    with open(tform_file, 'r') as f:
        to_invivo = map(float, [x.rstrip() for x in f.readlines()])
    
    t = servermanager.CreateProxy("transforms", "Transform")
    tp = servermanager._getPyProxy(t)
    
    tp.Matrix = to_invivo
    
    # # create a new 'Transform'
    TForm_list.append(Transform(Input=Block_list[i]))
    TForm_list[i].Transform = tp

    # # Properties modified on transform1
    TForm_list[i].TransformAllInputVectors = 0
    #
    Hide(Block_list[i], renderView1)

    # # set active source
    # SetActiveSource(TForm_list[i])

    # # create a new 'Calculator'
    # Calc_list.append(Calculator(Input=TForm_list[i]))
    # Calc_list[i].Function = ''
    #
    # # Properties modified on Calc_list[i]
    # Calc_list[i].Function = 'mag(MetaImage)'
    #
    # # create a new 'Threshold'
    # Thresh_list.append(Threshold(Input=Calc_list[i]))
    # Thresh_list[i].Scalars = ['POINTS', 'Result']
    # Thresh_list[i].ThresholdRange = [0.0, 1.4312992259902995]
    #
    # # Properties modified on Thresh_list[i]
    # Thresh_list[i].ThresholdRange = [0.05725196903961198, 1.4312992259902995]

    # # show data in view
    # threshold1Display = Show(Thresh_list[i], renderView1)
    #
    # # update the view to ensure updated data information
    # renderView1.Update()
    #
    # # get color transfer function/color map for 'Result'
    # resultLUT = GetColorTransferFunction('Result')
    #
    # # get opacity transfer function/opacity map for 'Result'
    # resultPWF = GetOpacityTransferFunction('Result')
    #
    # # trace defaults for the display properties.
    # threshold1Display.Representation = 'Surface'
    # threshold1Display.ColorArrayName = ['POINTS', 'Result']
    # threshold1Display.LookupTable = resultLUT
    # threshold1Display.OSPRayScaleArray = 'Result'
    # threshold1Display.OSPRayScaleFunction = 'PiecewiseFunction'
    # threshold1Display.SelectOrientationVectors = 'MetaImage'
    # threshold1Display.ScaleFactor = 1.1743479011541618
    # threshold1Display.SelectScaleArray = 'Result'
    # threshold1Display.GlyphType = 'Arrow'
    # threshold1Display.GlyphTableIndexArray = 'Result'
    # threshold1Display.GaussianRadius = 0.05871739505770808
    # threshold1Display.SetScaleArray = ['POINTS', 'Result']
    # threshold1Display.ScaleTransferFunction = 'PiecewiseFunction'
    # threshold1Display.OpacityArray = ['POINTS', 'Result']
    # threshold1Display.OpacityTransferFunction = 'PiecewiseFunction'
    # threshold1Display.DataAxesGrid = 'GridAxesRepresentation'
    # threshold1Display.SelectionCellLabelFontFile = ''
    # threshold1Display.SelectionPointLabelFontFile = ''
    # threshold1Display.PolarAxes = 'PolarAxesRepresentation'
    # threshold1Display.ScalarOpacityFunction = resultPWF
    # threshold1Display.ScalarOpacityUnitDistance = 0.6166903106142697
    #
    # # init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
    # threshold1Display.DataAxesGrid.XTitleFontFile = ''
    # threshold1Display.DataAxesGrid.YTitleFontFile = ''
    # threshold1Display.DataAxesGrid.ZTitleFontFile = ''
    # threshold1Display.DataAxesGrid.XLabelFontFile = ''
    # threshold1Display.DataAxesGrid.YLabelFontFile = ''
    # threshold1Display.DataAxesGrid.ZLabelFontFile = ''
    #
    # # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
    # threshold1Display.PolarAxes.PolarAxisTitleFontFile = ''
    # threshold1Display.PolarAxes.PolarAxisLabelFontFile = ''
    # threshold1Display.PolarAxes.LastRadialAxisTextFontFile = ''
    # threshold1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''
    #
    # # show color bar/color legend
    # threshold1Display.SetScalarBarVisibility(renderView1, True)
    #
    # # update the view to ensure updated data information
    # renderView1.Update()
    #
    # # Properties modified on threshold1Display
    # threshold1Display.MapScalars = 0
    #
    # # set scalar coloring
    # ColorBy(threshold1Display, ('POINTS', 'MetaImage', 'Magnitude'))

# create a new 'Group Datasets'
groupDatasets1 = GroupDatasets(Input=TForm_list)

# # find source
# metaFileSeriesReader3 = FindSource('MetaFileSeriesReader3')
#
# # find source
# metaFileSeriesReader2 = FindSource('MetaFileSeriesReader2')
#
# # find source
# metaFileSeriesReader1 = FindSource('MetaFileSeriesReader1')
#
# # find source
# calculator2 = FindSource('Calculator2')

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1876, 1278]

# show data in view
groupDatasets1Display = Show(groupDatasets1, renderView1)

# get color transfer function/color map for 'Result'
resultLUT = GetColorTransferFunction('Result')

# get opacity transfer function/opacity map for 'Result'
resultPWF = GetOpacityTransferFunction('Result')

# trace defaults for the display properties.
groupDatasets1Display.Representation = 'Surface'
groupDatasets1Display.ColorArrayName = ['POINTS', 'Result']
groupDatasets1Display.LookupTable = resultLUT
groupDatasets1Display.OSPRayScaleArray = 'Result'
groupDatasets1Display.OSPRayScaleFunction = 'PiecewiseFunction'
groupDatasets1Display.SelectOrientationVectors = 'MetaImage'
groupDatasets1Display.ScaleFactor = 2.4824760199948637
groupDatasets1Display.SelectScaleArray = 'Result'
groupDatasets1Display.GlyphType = 'Arrow'
groupDatasets1Display.GlyphTableIndexArray = 'Result'
groupDatasets1Display.GaussianRadius = 0.12412380099974318
groupDatasets1Display.SetScaleArray = ['POINTS', 'Result']
groupDatasets1Display.ScaleTransferFunction = 'PiecewiseFunction'
groupDatasets1Display.OpacityArray = ['POINTS', 'Result']
groupDatasets1Display.OpacityTransferFunction = 'PiecewiseFunction'
groupDatasets1Display.DataAxesGrid = 'GridAxesRepresentation'
groupDatasets1Display.SelectionCellLabelFontFile = ''
groupDatasets1Display.SelectionPointLabelFontFile = ''
groupDatasets1Display.PolarAxes = 'PolarAxesRepresentation'
groupDatasets1Display.ScalarOpacityFunction = resultPWF
groupDatasets1Display.ScalarOpacityUnitDistance = 0.6226190969717862

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
groupDatasets1Display.DataAxesGrid.XTitleFontFile = ''
groupDatasets1Display.DataAxesGrid.YTitleFontFile = ''
groupDatasets1Display.DataAxesGrid.ZTitleFontFile = ''
groupDatasets1Display.DataAxesGrid.XLabelFontFile = ''
groupDatasets1Display.DataAxesGrid.YLabelFontFile = ''
groupDatasets1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
groupDatasets1Display.PolarAxes.PolarAxisTitleFontFile = ''
groupDatasets1Display.PolarAxes.PolarAxisLabelFontFile = ''
groupDatasets1Display.PolarAxes.LastRadialAxisTextFontFile = ''
groupDatasets1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# # hide data in view
# Hide(threshold2, renderView1)
#
# # hide data in view
# Hide(threshold1, renderView1)

# show color bar/color legend
groupDatasets1Display.SetScalarBarVisibility(renderView1, True)

# # find source
# transform1 = FindSource('Transform1')
#
# # find source
# calculator1 = FindSource('Calculator1')
#
# # find source
# transform2 = FindSource('Transform2')

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on groupDatasets1Display
groupDatasets1Display.MapScalars = 0

# set scalar coloring
ColorBy(groupDatasets1Display, ('POINTS', 'MetaImage', 'Magnitude'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(resultLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
groupDatasets1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
groupDatasets1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'MetaImage'
metaImageLUT = GetColorTransferFunction('MetaImage')

# get opacity transfer function/opacity map for 'MetaImage'
metaImagePWF = GetOpacityTransferFunction('MetaImage')

#
    # show data in view
    # transform1Display = Show(TForm_list[i], renderView1)
#
# # trace defaults for the display properties.
# transform1Display.Representation = 'Surface'
# transform1Display.ColorArrayName = ['POINTS', 'MetaImage']
# transform1Display.LookupTable = metaImageLUT
# transform1Display.OSPRayScaleArray = 'MetaImage'
# transform1Display.OSPRayScaleFunction = 'PiecewiseFunction'
# transform1Display.SelectOrientationVectors = 'MetaImage'
# transform1Display.ScaleFactor = 7.164656028041762
# transform1Display.SelectScaleArray = 'MetaImage'
# transform1Display.GlyphType = 'Arrow'
# transform1Display.GlyphTableIndexArray = 'MetaImage'
# transform1Display.GaussianRadius = 0.3582328014020881
# transform1Display.SetScaleArray = ['POINTS', 'MetaImage']
# transform1Display.ScaleTransferFunction = 'PiecewiseFunction'
# transform1Display.OpacityArray = ['POINTS', 'MetaImage']
# transform1Display.OpacityTransferFunction = 'PiecewiseFunction'
# transform1Display.DataAxesGrid = 'GridAxesRepresentation'
# transform1Display.SelectionCellLabelFontFile = ''
# transform1Display.SelectionPointLabelFontFile = ''
# transform1Display.PolarAxes = 'PolarAxesRepresentation'
# transform1Display.ScalarOpacityFunction = metaImagePWF
# transform1Display.ScalarOpacityUnitDistance = 0.4790483739770441
#
# # init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
# transform1Display.DataAxesGrid.XTitleFontFile = ''
# transform1Display.DataAxesGrid.YTitleFontFile = ''
# transform1Display.DataAxesGrid.ZTitleFontFile = ''
# transform1Display.DataAxesGrid.XLabelFontFile = ''
# transform1Display.DataAxesGrid.YLabelFontFile = ''
# transform1Display.DataAxesGrid.ZLabelFontFile = ''
#
# # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
# transform1Display.PolarAxes.PolarAxisTitleFontFile = ''
# transform1Display.PolarAxes.PolarAxisLabelFontFile = ''
# transform1Display.PolarAxes.LastRadialAxisTextFontFile = ''
# transform1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''
#
# # show color bar/color legend
# transform1Display.SetScalarBarVisibility(renderView1, True)
#
# # hide data in view
# Hide(iMG_020_surfacemhd, renderView1)
#
# # Properties modified on transform1Display
# transform1Display.MapScalars = 0
#
# # # Properties modified on transform1
# transform1.TransformAllInputVectors = 0

# # update the view to ensure updated data information
# renderView1.Update()

# # create a new 'Resample With Dataset'
# resampleWithDataset1 = ResampleWithDataset(Input=MR_Data,
#     Source=None)
# resampleWithDataset1.CellLocator = 'Static Cell Locator'

# # set active source
# SetActiveSource(MR_Data)

# # destroy resampleWithDataset1
# Delete(resampleWithDataset1)
# del resampleWithDataset1

# # set active source
# SetActiveSource(transform1)

# # create a new 'Resample With Dataset'
# resampleWithDataset1 = ResampleWithDataset(Input=MR_Data,
#     Source=transform1)
# resampleWithDataset1.CellLocator = 'Static Cell Locator'
#
# # # show data in view
# resampleWithDataset1Display = Show(resampleWithDataset1, renderView1)
#
# # trace defaults for the display properties.
# resampleWithDataset1Display.Representation = 'Surface'
# resampleWithDataset1Display.ColorArrayName = ['POINTS', 'MetaImage']
# resampleWithDataset1Display.LookupTable = metaImageLUT
# resampleWithDataset1Display.OSPRayScaleArray = 'MetaImage'
# resampleWithDataset1Display.OSPRayScaleFunction = 'PiecewiseFunction'
# resampleWithDataset1Display.SelectOrientationVectors = 'MetaImage'
# resampleWithDataset1Display.ScaleFactor = 7.164656028041762
# resampleWithDataset1Display.SelectScaleArray = 'MetaImage'
# resampleWithDataset1Display.GlyphType = 'Arrow'
# resampleWithDataset1Display.GlyphTableIndexArray = 'MetaImage'
# resampleWithDataset1Display.GaussianRadius = 0.3582328014020881
# resampleWithDataset1Display.SetScaleArray = ['POINTS', 'MetaImage']
# resampleWithDataset1Display.ScaleTransferFunction = 'PiecewiseFunction'
# resampleWithDataset1Display.OpacityArray = ['POINTS', 'MetaImage']
# resampleWithDataset1Display.OpacityTransferFunction = 'PiecewiseFunction'
# resampleWithDataset1Display.DataAxesGrid = 'GridAxesRepresentation'
# resampleWithDataset1Display.SelectionCellLabelFontFile = ''
# resampleWithDataset1Display.SelectionPointLabelFontFile = ''
# resampleWithDataset1Display.PolarAxes = 'PolarAxesRepresentation'
# resampleWithDataset1Display.ScalarOpacityFunction = metaImagePWF
# resampleWithDataset1Display.ScalarOpacityUnitDistance = 0.4790483739770441
#
# # init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
# resampleWithDataset1Display.DataAxesGrid.XTitleFontFile = ''
# resampleWithDataset1Display.DataAxesGrid.YTitleFontFile = ''
# resampleWithDataset1Display.DataAxesGrid.ZTitleFontFile = ''
# resampleWithDataset1Display.DataAxesGrid.XLabelFontFile = ''
# resampleWithDataset1Display.DataAxesGrid.YLabelFontFile = ''
# resampleWithDataset1Display.DataAxesGrid.ZLabelFontFile = ''
#
# # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
# resampleWithDataset1Display.PolarAxes.PolarAxisTitleFontFile = ''
# resampleWithDataset1Display.PolarAxes.PolarAxisLabelFontFile = ''
# resampleWithDataset1Display.PolarAxes.LastRadialAxisTextFontFile = ''
# resampleWithDataset1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''
#
# # hide data in view
# Hide(MR_Data, renderView1)
#
# # hide data in view
# Hide(transform1, renderView1)
#
# # show color bar/color legend
# resampleWithDataset1Display.SetScalarBarVisibility(renderView1, True)
#
# # update the view to ensure updated data information
# renderView1.Update()
#
# # Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
# metaImageLUT.ApplyPreset('Grayscale', True)
#
# # set active source
# SetActiveSource(transform1)
#
# # show data in view
# transform1Display = Show(transform1, renderView1)
#
# # show color bar/color legend
# transform1Display.SetScalarBarVisibility(renderView1, True)
#
# # hide data in view
# Hide(resampleWithDataset1, renderView1)
#
# # set active source
# SetActiveSource(resampleWithDataset1)
#
# # show data in view
# resampleWithDataset1Display = Show(resampleWithDataset1, renderView1)
#
# # show color bar/color legend
# resampleWithDataset1Display.SetScalarBarVisibility(renderView1, True)
#
# # set active source
# SetActiveSource(resampleWithDataset1)
#
# renderView1.Update()