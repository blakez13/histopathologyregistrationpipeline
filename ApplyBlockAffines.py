from __future__ import print_function

import os
import sys
import glob
import pickle
import argparse
import numpy as np
import PyCA.Core as ca
import PyCAApps as apps
import RabbitCommon as rc
import PyCA.Common as common

import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend('Qt5Agg')

import PyCACalebExtras.Common as cc
import PyCABlakeExtras.Common as cb

plt.ion()
plt.close('all')
cc.SelectGPU(1)


def apply_affine(im, affine):

    # Now solve for the grid and create a field and image with that grid
    aff_grid = rc.SolveAffineGrid(im.grid(), affine)
    print(aff_grid)

    aff_im = ca.Image3D(aff_grid, im.memType())
    h = ca.Field3D(aff_grid, im.memType())

    # Now make the inverse transform into an h field
    cc.AtoHReal(h, np.linalg.inv(affine))
    cc.ApplyHReal(aff_im, im, h, bg=3)

    return aff_im


parser = argparse.ArgumentParser(description='Affine Apply ')
parser.add_argument('-b', '--block', type=str, help='Path to block image file', required=True)
parser.add_argument('-a', '--affine', type=str, help='Affine Transform', required=True)
parser.add_argument('-z', '--z_offset', type=float, help='Initial Z offset (blender)', default=None)
parser.add_argument('-o', '--output', type=str, help='Output file', required=True)
parser.add_argument('-m', '--mask', type=str, help='Path ot block mask file')
args = parser.parse_args()

mem_type = ca.MEM_DEVICE

# Load the block
block = common.LoadITKImage(args.block, mem_type)

if args.z_offset:
    block.setOrigin(ca.Vec3Df(block.origin()[0], block.origin()[1], args.z_offset))
    temp_origin = block.origin().copy()
    block = cc.ResampleToSize(block, [block.size().x // 8, block.size().y // 8, block.size().z])
    block.setSpacing(ca.Vec3Df(block.spacing().x * 8, block.spacing().y * 8, block.spacing().z))
    block.setOrigin(temp_origin)

# Print the affine
affine = np.array([float(x) for x in args.affine.split(' ')[1:]]).reshape(4, 4)
# Apply the affine to the block
aff_block = apply_affine(block, affine)

# # If the mask is specified, load it and apply the affine and mask the block
# if args.mask is not None:
#     mask = common.LoadITKImage(args.block, mem_type)
#     # print('===> Applying affine to mask')
#     aff_mask = apply_affine(mask, affine)
#     ca.GTEC_I(aff_mask, 0.5)
#     out_block = aff_mask.copy() * aff_block.copy()

common.SaveITKImage(aff_block, args.output)
print('===> Done applying transforms')
