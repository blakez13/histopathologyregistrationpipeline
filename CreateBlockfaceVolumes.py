from __future__ import print_function

import os
import sys
import glob
import pickle
import argparse
import numpy as np
import PyCA.Core as ca
import PyCAApps as apps
import RabbitCommon as rc
import PyCA.Common as common
from collections import OrderedDict

import PyCACalebExtras.SetBackend

plt = PyCACalebExtras.SetBackend.SetBackend('Qt5Agg')

import PyCACalebExtras.Common as cc
import PyCABlakeExtras.Common as cb
import PyCACalebExtras.Display as cd

# from sklearn.svm import SVC
# from sklearn.gaussian_process import GaussianProcessClassifier as gpc
from sklearn.externals import joblib
from sklearn.neighbors import KNeighborsClassifier as knc

plt.ion()
plt.close('all')
cc.SelectGPU(1)


def read_mhd_header(filename):

    with open(filename, 'r') as in_mhd:
        long_string = in_mhd.read()

    short_strings = long_string.split('\n')
    key_list = [x.split(' = ')[0] for x in short_strings[:-1]]
    value_list = [x.split(' = ')[1] for x in short_strings[:-1]]
    a = OrderedDict(zip(key_list, value_list))

    return a


def write_mhd_header(filename, dictionary):
    long_string = '\n'.join(['{0} = {1}'.format(k, v) for k, v in dictionary.iteritems()])
    with open(filename, 'w+') as out:
        out.write(long_string)


def _initSolve(fileList, output, spacing=[1, 1, 1], Ainit=np.eye(3)):
    mType = ca.MEM_DEVICE
    #
    # depth = 0

    # Using Caleb's load TIFF flips the image so it looks as expected
    ImPrev = cc.LoadTIFF(fileList[0], mType)

    # Create the grid for the images
    # Because they are all cropped in PS, they should all have the same size
    grid = cc.MakeGrid(ImPrev.size(), spacing, 'center')
    ImPrev.setGrid(grid)

    # im_color = cc.LoadTIFF(fileList[0], mType, col='rgb')
    # out_name = output + 'IMG_{0}_depth_{1}_{2}.tiff'.format(str(depth // 50).zfill(3),
    #                                                         str(depth).zfill(4),
    #                                                         fileList[0].split('/')[-1].split('_')[-1].split('.')[0])
    # cc.WriteTIFF(im_color, out_name)
    # Also need to get the other image
    # block = firstImage.copy() #cc.SubVol(firstImage, yrng=[2080, 2900], xrng=[1530, 2200])

    # origin = [(block.grid().size().x+1)/2.0, # origin for Affine matrix
    #           (block.grid().size().y+1)/2.0,
    #           (block.grid().size().z+1)/2.0]

    Adict = {'origin': grid.origin().tolist(), 'spacing': grid.spacing().tolist(),
             fileList.index(fileList[0]): np.eye(3)}

    # imGrid = cc.MakeGrid(block.size(), spacing, 'center')
    # block.setGrid(imGrid)
    # ImPrev = block.copy()

    # Set the first affine to the identity matrix

    # Set the maximum number of iterations for the registration - For some reason it doesn't stop
    maxIter = 300

    for fileName in fileList[1:]:
        print('Registering {0} .... '.format(fileName), end='')
        sys.stdout.flush()

        # Get the number the file is from the start
        dist = fileList.index(fileName)

        # Load the next image
        Im = cc.LoadTIFF(fileName, mType)
        Im.setGrid(grid)

        [defIm, A, e] = apps.AffineReg(Im, ImPrev, A=Ainit, constraint='rigid', maxIter=maxIter, verbose=0, plot=False)

        Adict[dist] = A
        ImPrev = defIm.copy()

    return Adict


def _make_volume(args, directory):
    mhd_list = glob.glob(directory + '/*.mhd')

    mhd_dict = read_mhd_header(mhd_list[0])

    # Update the files to be loaded
    prefix = '{}'.format(directory.replace(args.output, ''))
    name = mhd_list[0].split('.')[0].split('_')[-1]
    data_files = prefix + 'IMG_%03d_{0}.raw 0 {1} 1'.format(name, len(mhd_list) - 1)
    mhd_dict['ElementDataFile'] = data_files

    # Update the dimensions
    dims = map(int, mhd_dict['DimSize'].split(' '))
    dims[2] = len(mhd_list)
    mhd_dict['DimSize'] = ' '.join(map(str, dims))

    # Update the spacing
    mhd_dict['ElementSpacing'] = ' '.join(map(str, args.spacing))

    write_mhd_header(args.output + name + '_volume.mhd', mhd_dict)


def _apply_affines(fileList, Adict, args):
    mType = ca.MEM_DEVICE

    print('Applying affines ... ', end='')
    sys.stdout.flush()

    # Split the list into the two types
    surface_list = [x for x in fileList if 'surface' in x]
    scatter_list = [x for x in fileList if 'scatter' in x]

    # Make the output directories
    if not os.path.exists(args.output + '/surface/'):
        os.makedirs(args.output + '/surface/')
    if not os.path.exists(args.output + '/scatter/'):
        os.makedirs(args.output + '/scatter/')
    if not os.path.exists(args.output + '/difference/'):
        os.makedirs(args.output + '/difference/')

    dist = 0

    for surface_name, scatter_name in zip(surface_list, scatter_list):
        surface_im = cc.LoadTIFF(surface_name, mType, col='rgb')
        scatter_im = cc.LoadTIFF(scatter_name, mType, col='rgb')

        imGrid = cc.MakeGrid(surface_im.size(), Adict['spacing'], Adict['origin'])

        surface_im.setGrid(imGrid)
        scatter_im.setGrid(imGrid)

        surface_affine = surface_im.copy()
        cc.ApplyAffineReal(surface_affine, surface_im, Adict[dist])

        scatter_affine = scatter_im.copy()
        cc.ApplyAffineReal(scatter_affine, scatter_im, Adict[dist])

        surface_affine /= ca.Max(surface_affine)
        scatter_affine /= ca.Max(scatter_affine)

        # difference /= ca.Max(difference)
        difference = scatter_affine - surface_affine

        common.SaveITKField(surface_affine,
                            args.output + '/surface/IMG_{0}_surface.mhd'.format(str(dist).zfill(3),
                                                                                          str(dist).zfill(4)))
        common.SaveITKField(scatter_affine,
                            args.output + '/scatter/IMG_{0}_scatter.mhd'.format(str(dist).zfill(3),
                                                                                          str(dist).zfill(4)))
        common.SaveITKField(difference,
                            args.output + '/difference/IMG_{0}_difference.mhd'.format(
                                str(dist).zfill(3),
                                str(dist).zfill(4)))
        dist += 1

    print('Done')


def _hsvVolumes(fileList, Adict):
    mType = ca.MEM_DEVICE
    Hstack = []
    Sstack = []
    Vstack = []

    print('Generating HSV Volumes ... ', end='')
    sys.stdout.flush()

    for fileName in fileList:
        dist = fileList.index(fileName)
        Im = cc.LoadTIFF(fileName, mType, col='rgb')
        imGrid = cc.MakeGrid(Im.size(), Adict['spacing'], Adict['origin'])
        Im.setGrid(imGrid)

        HSV_or = cb.RGBtoHSV2D(Im)

        HSV = cc.SubVol(HSV_or, yrng=[174, 1096], xrng=[142, 818])

        H = ca.Image3D(HSV.grid(), HSV.memType())
        S = ca.Image3D(HSV.grid(), HSV.memType())
        V = ca.Image3D(HSV.grid(), HSV.memType())

        ca.Copy(H, HSV, 0)
        ca.Copy(S, HSV, 1)
        ca.Copy(V, HSV, 2)

        affIm = H.copy()
        cc.ApplyAffineReal(affIm, H, Adict[dist])
        Hstack.append(affIm.copy())
        cc.ApplyAffineReal(affIm, S, Adict[dist])
        Sstack.append(affIm.copy())
        cc.ApplyAffineReal(affIm, V, Adict[dist])
        Vstack.append(affIm.copy())

    Hvol = cc.Imlist_to_Im(Hstack)
    Svol = cc.Imlist_to_Im(Sstack)
    Vvol = cc.Imlist_to_Im(Vstack)

    print('Done')

    return Hvol, Svol, Vvol


def _correction(fileList, Adict, badNum):
    mType = ca.MEM_DEVICE

    I_tar_raw = cc.LoadTIFF(fileList[badNum - 1], ca.MEM_HOST)
    I_src = cc.LoadTIFF(fileList[badNum], ca.MEM_HOST)

    imGrid = cc.MakeGrid(I_tar_raw.size(), Adict['spacing'], Adict['origin'])

    I_tar_raw.setGrid(imGrid)
    I_src.setGrid(imGrid)

    I_tar = I_tar_raw.copy()
    cc.ApplyAffineReal(I_tar, I_tar_raw, Adict[badNum - 1])

    landmarks = rc.LandmarkPicker([np.squeeze(I_src.asnp()), np.squeeze(I_tar.asnp())])
    for lm in landmarks:
        lm[0] = np.ndarray.tolist(np.multiply(lm[0], I_src.spacing().tolist()[0:2]) + I_src.origin().tolist()[0:2])
        lm[1] = np.ndarray.tolist(np.multiply(lm[1], I_tar.spacing().tolist()[0:2]) + I_tar.origin().tolist()[0:2])
    aff = apps.SolveAffine(landmarks)

    ImPrev = I_tar.copy()
    ImPrev.toType(mType)

    maxIter = 300

    for fileName in fileList[badNum:]:
        print('Registering {0} .... '.format(fileName), end='')
        sys.stdout.flush()

        # Get the number the file is from the start
        dist = fileList.index(fileName)

        # Load the next image
        Im = cc.LoadTIFF(fileName, mType)
        Im.setGrid(imGrid)

        [defIm, A, _] = apps.AffineReg(Im, ImPrev, A=aff, constraint='rigid', maxIter=maxIter, verbose=0, plot=False)

        Adict[dist] = A
        ImPrev = defIm.copy()

    return Adict


parser = argparse.ArgumentParser(description='Stacking ')
parser.add_argument('-i', '--input', type=str, help='Input directory for the blockface images', required=True)
parser.add_argument('-o', '--output', type=str, help='output directory for the affine images', required=True)
parser.add_argument('-s', '--spacing', nargs='+', type=float, help='List of the x, y, z spacing for the block if not '
                                                                   'already defined')
parser.add_argument('-c', '--correction', type=int, help='The slice number that out of alignment (Assumes 0 indexing)')
parser.add_argument('-p', '--process', type=str, help='Type of pre-processed image to use')
args = parser.parse_args()

Dir = args.input
Out = args.output

# Check that there actually is data in the base directory
if not os.path.exists(Dir) or glob.glob(Dir + '*') == []:
    sys.exit("There doesn't appear to be any blockface data for this rabbit")

# Get the images that need to be turned into a volume
imageList = sorted(glob.glob(Dir + '/*.tif'))

# Check that there actually are images for the block requested
if not imageList:
    sys.exit("There doesn't appear to be any images for this block")

if not os.path.exists(Out):
    os.makedirs(Out)

# Try opening the configuration file if the block has already been processed - If not run initSolve
try:
    with open(Out + 'blockface_affines.pkl', 'r') as f:
        Adict = pickle.load(f)
        f.close()
except IOError:
    if args.spacing is None:
        sys.exit('A spacing needs to be provided for this block. Use "-s x y z" to define the spacing.')

    # Now that there will be 2 types of images in there
    subList = [x for x in imageList if 'surface' in x]
    Adict = _initSolve(subList, Out, args.spacing)

    with open(Out + 'blockface_affines.pkl', 'w') as f:
        pickle.dump(Adict, f)

if not glob.glob(args.output + '/difference/'):
    _apply_affines(imageList, Adict, args)

    print('Making Volumes ... ', end='')
    _make_volume(args, args.output + 'surface/')
    _make_volume(args, args.output + 'scatter/')
    _make_volume(args, args.output + 'difference/')
    print('Done')
# # Make sure there is a directory to write the volumes to
# if not os.path.exists(Dir + '../../volumes/block{0}'.format(str(args.block).zfill(2))):
# 	os.makedirs(Dir + '../../volumes/block{0}'.format(str(args.block).zfill(2)))

# if not os.path.exists(Dir + '../../volumes/block{0}/block{0}_volume_surface.nrrd'.format(str(args.block).zfill(2))):
# 	volumes, colors = _applyAndStack(imageList, Adict, args, Dir)
#
# 	common.SaveITKImage(volumes[0], Dir + '../../volumes/block{0}/block{0}_volume_surface.nrrd'.format(str(args.block).zfill(2)))
# 	cc.WriteColorMHA(colors[0],  Dir + '../../volumes/block{0}/block{0}_volume_surface_color.mha'.format(str(args.block).zfill(2)))
#
# 	common.SaveITKImage(volumes[1], Dir + '../../volumes/block{0}/block{0}_volume_scatter.nrrd'.format(str(args.block).zfill(2)))
# 	cc.WriteColorMHA(colors[1],  Dir + '../../volumes/block{0}/block{0}_volume_scatter_color.mha'.format(str(args.block).zfill(2)))
#
# 	common.SaveITKImage(volumes[1] - volumes[0], Dir + '../../volumes/block{0}/block{0}_difference.nrrd'.format(str(args.block).zfill(2)))
# 	cc.WriteColorMHA(colors[1] - colors[0],  Dir + '../../volumes/block{0}/block{0}_volume_difference_color.mha'.format(str(args.block).zfill(2)))
#
# 	with open(Dir + '../../affineImages/block{0}/block{0}_affines.pkl'.format(str(args.block).zfill(2)), 'w') as f:
# 		pickle.dump(Adict, f)

# if os.path.exists(Dir + '../../volumes/block{0}/block{0}_volume.nrrd'.format(str(args.block).zfill(2))) and Adict:
# 	print('Affine solving and stacking complete.')

if args.correction is not None:
    subList = [x for x in imageList if 'surface' in x]
    corDict = _correction(subList, Adict, args.correction - 1)

    with open(Out + 'blockface_affines.pkl', 'w') as f:
        pickle.dump(corDict, f)
        f.close()

    _apply_affines(imageList, corDict, args)

# volumes, colors = _applyAndStack(imageList, corDict, args, Dir)

# common.SaveITKImage(volumes[0],
# 					Dir + '../../volumes/block{0}/block{0}_volume_surface.nrrd'.format(str(args.block).zfill(2)))
# cc.WriteColorMHA(colors[0],
# 				 Dir + '../../volumes/block{0}/block{0}_volume_surface_color.mha'.format(str(args.block).zfill(2)))
#
# common.SaveITKImage(volumes[1],
# 					Dir + '../../volumes/block{0}/block{0}_volume_scatter.nrrd'.format(str(args.block).zfill(2)))
# cc.WriteColorMHA(colors[1],
# 				 Dir + '../../volumes/block{0}/block{0}_volume_scatter_color.mha'.format(str(args.block).zfill(2)))
#
# common.SaveITKImage(volumes[1] - volumes[0],
# 					Dir + '../../volumes/block{0}/block{0}_difference.nrrd'.format(str(args.block).zfill(2)))
# cc.WriteColorMHA(colors[1] - colors[0], Dir + '../../volumes/block{0}/block{0}_volume_difference_color.mha'.format(
# 	str(args.block).zfill(2)))
