import os
import glob
import argparse
import numpy as np
import subprocess as sp

import matplotlib
matplotlib.use('qt5agg')

import matplotlib.pyplot as plt
plt.ion()

from PIL import Image

# Training settings
parser = argparse.ArgumentParser(description='PyTorch Super Res Example')
parser.add_argument('-u', '--unsorted', type=str, required=True, help="Directory to unsorted files")
parser.add_argument('-r', '--rabbit', type=str, required=True, help="Rabbit to reconstruct")
parser.add_argument('-b', '--blockface', type=str, required=True, help="Path to the blockface directory")

args = parser.parse_args()


def run(args):

    # Get a list of the images that are not sorted
    raw_paths = sorted(glob.glob(args.unsorted + '*'))
    pt1_paths = [x for x in raw_paths if 'pt1' in x]

    for path in pt1_paths[42:]:
        related = [x for x in raw_paths if path.split('_pt1')[0] in x]


        pt1 = [x for x in related if 'pt1' in x][0]
        related.remove([x for x in related if 'pt1' in x][0])
        pt2 = [x for x in related if 'pt2' in x][0]
        related.remove([x for x in related if 'pt2' in x][0])
        img = related

        print(related)

        if len(related) != 1:
            s_num = input(f'Pick which piece to keep ({related[0]}):  ')
            img = [x for x in related if s_num in x][0]
        else:
            img = related[0]

        label = Image.open(pt1)
        plt.imshow(np.array(label))
        plt.pause(0.001)
        block = input('Please enter the corresponding block: ')

        if block != 'skip':
            img_num = input('Please input the corresponding image:  ')

            # Make the names for the output
            if not os.path.exists(f'{args.unsorted}../raw/{block}'):
                os.makedirs(f'{args.unsorted}../raw/{block}')

            if not os.path.exists(f'{args.blockface}../microscopic_reference/{block}/images/'):
                os.makedirs(f'{args.blockface}../microscopic_reference/{block}/images/')

            mic_name = f'IMG_{int(img_num):03d}_histopathology_image.tif'
            lbl_name = f'IMG_{int(img_num):03d}_histopathology_label.tif'

            p = sp.Popen(["cp",
                          f"{img}",
                          f"{args.unsorted}../raw/{block}/{mic_name}"],
                         stdout=sp.PIPE)
            sout, _ = p.communicate()
            p.wait()

            p = sp.Popen(["cp",
                          f"{pt1}",
                          f"{args.unsorted}../raw/{block}/{lbl_name}"],
                         stdout=sp.PIPE)
            sout, _ = p.communicate()
            p.wait()

            block_imgs = sorted(glob.glob(f'{args.blockface}{block}/difference/*{img_num}*'))
            p = sp.Popen(["cp",
                          f"{block_imgs[0]}",
                          f"{args.blockface}../microscopic_reference/{block}/images/"],
                         stdout=sp.PIPE)
            sout, _ = p.communicate()
            p.wait()
            p = sp.Popen(["cp",
                          f"{block_imgs[1]}",
                          f"{args.blockface}../microscopic_reference/{block}/images/"],
                         stdout=sp.PIPE)
            sout, _ = p.communicate()
            p.wait()

            plt.close('all')

        else:
            pass


if __name__ == '__main__':
    run(args)
