import os
import sys
import glob
import VNet
import json
import pickle
import argparse
import subprocess as sp
import GenerateVNetData
from types import SimpleNamespace

parser = argparse.ArgumentParser(description='Histopathology Processing ')
parser.add_argument('-r', '--rabbit', type=str, help='Rabbit number for processing')
parser.add_argument('-b', '--block', type=int, help='Which block of tissue to reconstruct')
parser.add_argument('-s', '--spacing', nargs='+', type=float,
                    help='List of the x, y, z spacing for the block if not already defined')
parser.add_argument('--train', action='store_true', help='train vnet?')
parser.add_argument('--eval', action='store_true', help='eval vnet?')
parser.add_argument('--iso', action='store_true', help='generate isosurfaces?')

args = parser.parse_args()


def _eval_vnet(rabbit, block, spacing):

    Dir = f'/home/sci/blakez/ucair/histopathology/{rabbit}/blockface/'
    image_dir = Dir + f"affineImages/block{block}"
    out_name = Dir + f"volumes/block{block}/block{block}_predicted_mask.nrrd"

    try:
        # have to load a picle file that was created with python2
        with open(Dir + f'affineImages/block{block}/block{block}_affines.pkl', 'rb') as f:
            Adict = pickle.load(f, encoding='latin1')
            f.close()

    except IOError:
        # Assume the pre-processing affine stacking step has not been run

        if args.spacing is None:
            sys.exit('A spacing needs to be provided for this block. Use "-s x y z" to define the spacing.')

        p = sp.Popen(["/usr/bin/python",
                      "-u",
                      "/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/CreateBlockfaceVolumes.py",
                      "-r", f"{rabbit}",
                      "-b", f"{block}",
                      "-s", f"{spacing[0]}", f"{spacing[1]}", f"{spacing[2]}"],
                     stdout=sp.PIPE)
        sout, _ = p.communicate()
        p.wait()

        print(sout.decode('utf-8'))
        sys.stdout.flush()

        with open(Dir + f'affineImages/block{block}/block{block}_affines.pkl', 'rb') as f:
            Adict = pickle.load(f, encoding='latin1')
            f.close()

    evalOpt = {'evalBatchSize': 16,
               'image_dir': image_dir,
               'cuda': True,
               'threads': 0,
               'cube': [64, 128, 128],
               'ckpt': '/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/VNet/output/2019-02-13-200441/epoch_65_model.pth',
               'spacing': Adict['spacing'],
               'size': Adict['size'],
               'origin': Adict['origin'],
               'out_name': out_name
               }

    evalOpt = SimpleNamespace(**evalOpt)

    VNet.vNetMaster.eval(evalOpt)


def _train_vnet(blockList):

    # make sure we have all the training data we can
    GenerateVNetData.generate(blockList)

    trainOpt = {'trainBatchSize': 16,
                'inferBatchSize': 16,
                'nEpochs': 1000,
                'lr': 0.0001,
                'cuda': True,
                'threads': 16,
                'cube': [64, 128, 128],
                'resume': False,
                'scheduler': True,
                'ckpt': '/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/VNet/output/2019-02-08-182207/epoch_210_model.pth',
                }
    trainOpt = SimpleNamespace(**trainOpt)
    VNet.vNetMaster.train(trainOpt)


def _generate_isosurfaces(blockList):

    def _check(list):
        """ Function for checking if the mask exists and if there is already an obj file """

        # Get rid of any block that don't have a mask
        list = [x for x in list if os.path.exists(x + f'/{x.split("/")[-1]}_mask.nrrd')]

        # Remove and blocks that already have an obj file from seg3d
        list = [x for x in list if not os.path.exists(x + f'/{x.split("/")[-1]}_surface_seg3D.obj')]

        return list

    # Need to check that a mask has been created and no obj file has been made yet
    blockList = _check(blockList)

    for block in blockList:

        inFile = block + f'/{block.split("/")[-1]}_mask.nrrd'
        outFile = block + f'/{block.split("/")[-1]}_surface_seg3D.obj'
        # First create and write the json file to be read
        params = {'inputF': inFile,
                  'upperT': 1.0,
                  'lowerT': 0.1,
                  'outputF': outFile}

        with open('/home/sci/blakez/ucair/histopathology/generate_isosurface_temp.json', 'w') as fp:
            json.dump(params, fp)

        p = sp.Popen(["/home/sci/blakez/software/Seg3D-Master/bin-eiger/Seg3D/Seg3D2",
                      "python=/home/sci/blakez/ucair/HistopathologyRegistrationPipeline/GenerateIsosurfaceSeg3D.py",
                      "--headless"],
                     stdout=sp.PIPE)
        sout, _ = p.communicate()
        p.wait()

    # Remove the temp file when we are done
    os.remove('/home/sci/blakez/ucair/histopathology/generate_isosurface_temp.json')


# Get a list of all the block that have a mask - that way we can do things with them
Dir = '/home/sci/blakez/ucair/histopathology/'
# Get a list of all the blocks for all the rabbits
rabbits = sorted(glob.glob(Dir + '/18*'))
out = '/home/sci/blakez/ucair/histopathology/vnetData/'
blockList = []

# Get a list of all the block
for rabbit in rabbits:
    blockList += sorted(glob.glob(rabbit + '/blockface/volumes/*'))

if args.iso:
    _generate_isosurfaces(blockList)

if args.eval:
    _eval_vnet(args.rabbit, args.block, args.spacing)

if args.train:
    _train_vnet(blockList)

# To create the isosurfaces, use seg3d and write out a consisten json file that can be read by GenerateIsosurface

# Check to see if any files don't have an obj file

